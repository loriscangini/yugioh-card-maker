﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace YuGiOhCardMaker
{
    static class Utils
    {
        public static string Text(this RichTextBox box) => box.Document.Blocks.OfType<Paragraph>().Aggregate("", (result, p) => result += p.Text());
        public static string Text(this Paragraph p) => p.Inlines.OfType<Run>().Aggregate("", (total, r) => total += r.Text);

        public static double Angle(this LinearGradientBrush lgb) => Math.Atan2(lgb.EndPoint.Y - lgb.StartPoint.Y, lgb.EndPoint.X - lgb.StartPoint.X) * 180 / Math.PI;

        public static SolidColorBrush ToSolidBrush(this string text) => (SolidColorBrush)new BrushConverter().ConvertFrom(text);
        public static Color ToColor(this string text) => text.ToSolidBrush().Color;
    }
}

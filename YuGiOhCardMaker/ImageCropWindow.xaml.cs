﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace YuGiOhCardMaker
{
    /// <summary>
    /// Logica di interazione per ImageCropWindow.xaml
    /// </summary>
    public partial class ImageCropWindow : Window
    {
        private BitmapSource original;

        double Cap(double x, double max)
        {
            return Math.Max(Math.Min(x, max), 0);
        }

        public double X0
        {
            get => updownX0.Value.GetValueOrDefault(0);
            private set { 
                updownX0.Value = Cap(value, imgImage.ActualWidth);
            }
        }
        public double Y0
        {
            get => updownY0.Value.GetValueOrDefault(0);
            private set { 
                updownY0.Value = Cap(value, imgImage.ActualHeight);
            }
        }
        public double X1
        {
            get => updownX1.Value.GetValueOrDefault(0);
            private set { 
                updownX1.Value = Cap(value, imgImage.ActualWidth);
            }
        }
        public double Y1
        {
            get => updownY1.Value.GetValueOrDefault(0);
            private set { 
                updownY1.Value = Cap(value, imgImage.ActualHeight);
            }
        }

        public ImageCropWindow(BitmapSource image)
        {
            InitializeComponent();
            imgImage.Source = image;
            original = image;
        }

        private double Scale => original.PixelHeight / imgImage.MaxHeight;

        public CroppedBitmap CroppedImage => new CroppedBitmap(original, new Int32Rect(
            (int)(rectangle.Margin.Left * Scale),
            (int)(rectangle.Margin.Top * Scale),
            (int)(rectangle.Width * Scale),
            (int)(rectangle.Height * Scale)));

        private void OK_Click(object sender, RoutedEventArgs e) => Close();

        private void FullImage_Checked(object sender, RoutedEventArgs e)
        {
            X0 = 0;
            Y0 = 0;
            X1 = imgImage.ActualWidth;
            Y1 = imgImage.ActualHeight;

            //rectangle.Margin = new Thickness(0);
            //rectangle.Width = imgImage.ActualWidth;
            //rectangle.Height = imgImage.ActualHeight;

            UpdateResult();
        }

        private void ImgImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            radioFull.IsChecked = true;
        }

        private void UpdateResult()
        {
            try
            {
                result.Source = CroppedImage;
            }
            catch { }
        }

        Point firstClickLocation = new Point();

        private void ImgImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // Save the start point.
            firstClickLocation = e.GetPosition(imgImage);

            if (radioDefine.IsChecked == true)
            {
                X0 = firstClickLocation.X;
                Y0 = firstClickLocation.Y;
                X1 = X0;
                Y1 = Y0;
            }

            Mouse.Capture(imgImage);
        }

        private void ImgImage_MouseMove(object sender, MouseEventArgs e)
        {
            // Do nothing it we're not selecting an area.
            if (Mouse.LeftButton == MouseButtonState.Released) return;

            if (radioDefine.IsChecked == true)
            {
                // Save the new point.
                X1 = e.GetPosition(imgImage).X;
                Y1 = e.GetPosition(imgImage).Y;

                if (chk11.IsChecked == true)
                {
                    double d = Math.Max(Math.Abs(X0 - X1), Math.Abs(Y0 - Y1));

                    X1 = X0 - d * Math.Sign(X0 - X1);
                    Y1 = Y0 - d * Math.Sign(Y0 - Y1);
                }

                X1 = Math.Max(X1, 0);
                Y1 = Math.Max(Y1, 0);

                radioFull.IsChecked = false;
            }
            else if (radioMove.IsChecked == true) 
            {
                double deltaX = firstClickLocation.X - e.GetPosition(imgImage).X;
                double deltaY = firstClickLocation.Y - e.GetPosition(imgImage).Y;
                firstClickLocation = e.GetPosition(imgImage);

                X0 -= deltaX;
                Y0 -= deltaY;
                X1 -= deltaX;
                Y1 -= deltaY;
            }

            UpdateResult();
        }

        private void DrawRectangle()
        {
            rectangle.Margin = new Thickness(Math.Max(0, Math.Min(X0, X1)), Math.Max(0, Math.Min(Y0, Y1)), 0, 0);
            rectangle.Width = Math.Min(Math.Abs(X0 - X1), imgImage.ActualWidth - rectangle.Margin.Left);
            rectangle.Height = Math.Min(Math.Abs(Y0 - Y1), imgImage.ActualHeight - rectangle.Margin.Top);
        }

        private void ImgImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            imgImage.ReleaseMouseCapture();
            if (radioDefine.IsChecked == true)
                radioMove.IsChecked = true;
        }

        private void Updown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            radioFull.IsChecked = false;
            updownWidth.Value = Math.Abs(X0 - X1);
            updownHeight.Value = Math.Abs(Y0 - Y1);
            DrawRectangle();
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Media;
using System.Windows;
using System.Windows.Documents;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace YuGiOhCardMaker
{
    /// <summary>
    /// OutlineText custom control class derives layout, event, data binding, and rendering from derived FrameworkElement class.
    /// </summary>
    public class OutlineTextControl : FrameworkElement
    {
        #region Private Fields

        private System.Collections.Generic.List<Geometry> textGeometries = new System.Collections.Generic.List<Geometry>();

        #endregion

        #region Private Methods

        /// <summary>
        /// Invoked when a dependency property has changed. Generate a new FormattedText object to display.
        /// </summary>
        /// <param name="d">OutlineText object whose property was updated.</param>
        /// <param name="e">Event arguments for the dependency property.</param>
        [Obsolete]
        private static void OnOutlineTextInvalidated(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((OutlineTextControl)d).CreateText();
        }

        #endregion

        #region FrameworkElement Overrides
        //<SnippetOnRender>
        /// <summary>
        /// OnRender override draws the geometry of the text and optional highlight.
        /// </summary>
        /// <param name="drawingContext">Drawing context of the OutlineText control.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            Geometry combined = Geometry.Empty;

            textGeometries.ForEach(g => {
                combined = new CombinedGeometry(GeometryCombineMode.Union, combined, g);
            });

            // Draw the outline based on the properties that are set.
            drawingContext.DrawGeometry(Fill, new Pen(Stroke, StrokeThickness), combined);
            drawingContext.DrawGeometry(Fill, new Pen(), combined);
        }

        //</SnippetOnRender>
        //<SnippetCreateText>
        /// <summary>
        /// Create the outline geometry based on the formatted text.
        /// </summary>
        [Obsolete]
        public void CreateText()
        {
            textGeometries.Clear();

            if (Paragraph == null) return;

            IEnumerable<FormattedText> fts = Paragraph.Inlines.OfType<Run>().Select(r => new FormattedText(
                    r.Text, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                    new Typeface(r.FontFamily, r.FontStyle, r.FontWeight, r.FontStretch),
                    r.FontSize, r.Foreground));

            double totalwidth = 0;
            if (HorizontalPosition == HorizontalAlignment.Left)
                totalwidth = -fts.Sum(ft => ft.Width);

            foreach (FormattedText ft in fts)
            {
                Point origin = new System.Windows.Point(-ft.Width / 2, -ft.Height / 2);

                if (HorizontalPosition == HorizontalAlignment.Left)
                {
                    origin.X = totalwidth;
                    totalwidth += ft.Width;
                }
                if (HorizontalPosition == HorizontalAlignment.Right)
                {
                    origin.X = totalwidth;
                    totalwidth += ft.Width;
                }
                if (VerticalPosition == VerticalAlignment.Top)
                    origin.Y = -ft.Height;
                if (VerticalPosition == VerticalAlignment.Bottom)
                    origin.Y = 0;

                textGeometries.Add(ft.BuildGeometry(origin));
            }
        }
        //</SnippetCreateText>      
        #endregion

        #region DependencyProperties

        /// <summary>
        /// Specifies whether the font should display Bold font weight.
        /// </summary>
        public FontWeight FontWeight
        {
            get
            {
                return (FontWeight)GetValue(FontWeightProperty);
            }

            set
            {
                SetValue(FontWeightProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Bold dependency property.
        /// </summary>
        public static readonly DependencyProperty FontWeightProperty = DependencyProperty.Register(
            "FontWeight",
            typeof(FontWeight),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                FontWeights.Normal,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        public VerticalAlignment VerticalPosition
        {
            get => (VerticalAlignment)GetValue(VerticalPositionProperty);
            set => SetValue(VerticalPositionProperty, value);
        }

        /// <summary>
        /// Identifies the VerticalPosition dependency property.
        /// </summary>
        public static readonly DependencyProperty VerticalPositionProperty = DependencyProperty.Register(
            "VerticalPosition",
            typeof(VerticalAlignment),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                VerticalAlignment.Center,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        public HorizontalAlignment HorizontalPosition
        {
            get => (HorizontalAlignment)GetValue(HorizontalPositionProperty);
            set => SetValue(HorizontalPositionProperty, value);
        }

        /// <summary>
        /// Identifies the HorizontalPosition dependency property.
        /// </summary>
        public static readonly DependencyProperty HorizontalPositionProperty = DependencyProperty.Register(
            "HorizontalPosition",
            typeof(HorizontalAlignment),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                HorizontalAlignment.Center,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        /// <summary>
        /// Specifies the brush to use for the fill of the formatted text.
        /// </summary>
        public System.Windows.Media.Brush Fill
        {
            get
            {
                return (System.Windows.Media.Brush)GetValue(FillProperty);
            }

            set
            {
                SetValue(FillProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Fill dependency property.
        /// </summary>
        public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
            "Fill",
            typeof(System.Windows.Media.Brush),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                new SolidColorBrush(Colors.LightSteelBlue),
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        /// <summary>
        /// The font to use for the displayed formatted text.
        /// </summary>
        public System.Windows.Media.FontFamily Font
        {
            get
            {
                return (System.Windows.Media.FontFamily)GetValue(FontProperty);
            }

            set
            {
                SetValue(FontProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Font dependency property.
        /// </summary>
        public static readonly DependencyProperty FontProperty = DependencyProperty.Register(
            "Font",
            typeof(System.Windows.Media.FontFamily),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                YuGiOhCardMaker.Managers.Fonts.RoGSanSerifStdB,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        /// <summary>
        /// The current font size.
        /// </summary>
        public double FontSize
        {
            get
            {
                return (double)GetValue(FontSizeProperty);
            }

            set
            {
                SetValue(FontSizeProperty, value);
            }
        }

        /// <summary>
        /// Identifies the FontSize dependency property.
        /// </summary>
        public static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register(
            "FontSize",
            typeof(double),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                 (double)48.0,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies whether the font should display Italic font style.
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                return (FontStyle)GetValue(FontStyleProperty);
            }

            set
            {
                SetValue(FontStyleProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Italic dependency property.
        /// </summary>
        public static readonly DependencyProperty FontStyleProperty = DependencyProperty.Register(
            "FontStyle",
            typeof(FontStyle),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                 FontStyles.Normal,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies the brush to use for the stroke and optional highlight of the formatted text.
        /// </summary>
        public System.Windows.Media.Brush Stroke
        {
            get
            {
                return (System.Windows.Media.Brush)GetValue(StrokeProperty);
            }

            set
            {
                SetValue(StrokeProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Stroke dependency property.
        /// </summary>
        public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
            "Stroke",
            typeof(System.Windows.Media.Brush),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                 new SolidColorBrush(Colors.White),
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        ///     The stroke thickness of the font.
        /// </summary>
        public ushort StrokeThickness
        {
            get
            {
                return (ushort)GetValue(StrokeThicknessProperty);
            }

            set
            {
                SetValue(StrokeThicknessProperty, value);
            }
        }

        /// <summary>
        /// Identifies the StrokeThickness dependency property.
        /// </summary>
        public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
            "StrokeThickness",
            typeof(ushort),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                 (ushort)0,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies the text string to display.
        /// </summary>
        public Paragraph Paragraph
        {
            get
            {
                return (Paragraph)GetValue(ParagraphProperty);
            }

            set
            {
                SetValue(ParagraphProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Text dependency property.
        /// </summary>
        public static readonly DependencyProperty ParagraphProperty = DependencyProperty.Register(
            "Paragraph",
            typeof(Paragraph),
            typeof(OutlineTextControl),
            new FrameworkPropertyMetadata(
                 null,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker
{
    public static class BindingValues
    {
        public static IEnumerable<string> SeriesList => Series.all;
        public static string DefaultSeries => Series.Series10;

        static string ToNiceString(this Types type) {
            switch (type)
            {
                case Types.BeastWarrior: return "Beast-Warrior";
                case Types.CreatorGod: return "Creator God";
                case Types.DivineBeast: return "Divine-Beast";
                case Types.SeaSerpent: return "Sea Serpent";
                case Types.WingedBeast: return "Winged Beast";
                case Types.BlackMagic: return "Black Magic";
                case Types.DragonMagic: return "Dragon Magic";
                case Types.IllusionMagic: return "Illusion Magic";
                case Types.SeaBeast: return "Sea Beast";
                case Types.WhiteMagic: return "White Magic";
                case Types.Qmarks: return "???";
                case Types.Spell: return "Spell Card";
                case Types.Trap: return "Trap Card";
                default: return type.ToString();
            }
        }
        public static string ToNiceString(this Abilities ability)
        {
            switch (ability)
            {
                case Abilities.DeckMaster: return "Deck Master";
                case Abilities.DarkTuner: return "Dark Tuner";
                default: return ability.ToString();
            }
        }
        public static string TypeAbilityString(this CardTypes type)
        {
            switch (type)
            {
                case CardTypes.DarkSynchro: return "Dark Synchro";
                case CardTypes.CustomMonster: return "Ability";
                case CardTypes.EvoluteConjoint: return "Evolute";
                case CardTypes.Obelisk:
                case CardTypes.Ra:
                case CardTypes.Slifer:
                case CardTypes.WickedGod:
                case CardTypes.LegendaryDragon:
                case CardTypes.ZArcFusion:
                case CardTypes.ZArcFusionSynchro:
                case CardTypes.ZArcFusionSynchroXyz:
                case CardTypes.OriginalDarkSynchro:
                case CardTypes.OriginalMonster: return "";
                case CardTypes.OriginalSpellTrap: return "Spell Card";
                case CardTypes.CustomSpellTrap: return "Spell/Trap Card";
                case CardTypes.Spell:
                case CardTypes.LinkSpell: return "Spell Card";
                case CardTypes.Trap:
                case CardTypes.LinkTrap: return "Trap Card";
                case CardTypes.Virus: return "Virus Card";
                case CardTypes.TrapSpell:
                case CardTypes.LinkTrapSpell: return "Trap Spell Card";
                case CardTypes.King: return "King Card";
                default: return type.ToString();
            }
        }
        public static IEnumerable<string> CardTypeNames => Enum.GetValues(typeof(CardTypes)).Cast<CardTypes>().Select(TypeAbilityString);
        public static IEnumerable<string> TypeNames => Enum.GetValues(typeof(Types)).Cast<Types>().Select(ToNiceString);
        public static IEnumerable<string> AbilityNames => Enum.GetValues(typeof(Abilities)).Cast<Abilities>().Select(ToNiceString);
        public static IEnumerable<string> SpellTrapTypes => new List<string>() { "Continuous", "Equip", "Field", "Quick-Play", "Ritual", "Counter Trap", "Splice", "Shield", "Arcade" };

        public static float Rank(this string ability)
        {
            switch (ability)
            {
                case "Synchro": return 0.1f;
                case "Xyz": return 0.2f;
                case "Pendulum":
                case "Deck Master": return 3;
                case "Toon":
                case "Spirit":
                case "Union":
                case "Gemini":
                case "Flip": return 4;
                case "Tuner":
                case "Dark Tuner":
                case "Base": return 5;
                case "Effect":
                case "Normal": return int.MaxValue;
                default: return CardTypeNames.Contains(ability) ? 0 : 1;
            }
        }

        public static HashSet<string> GetAbilities(this CardTypes type)
        {
            switch (type)
            {
                case CardTypes.Normal:
                case CardTypes.Token: return new HashSet<string>() { type.TypeAbilityString() };
                case CardTypes.Skill:
                case CardTypes.Spell:
                case CardTypes.Trap:
                case CardTypes.TrapSpell:
                case CardTypes.LinkSpell:
                case CardTypes.LinkTrap:
                case CardTypes.Virus:
                case CardTypes.OriginalSpellTrap:
                case CardTypes.CustomSpellTrap:
                case CardTypes.King:
                case CardTypes.Slifer:
                case CardTypes.Obelisk:
                case CardTypes.Ra:
                case CardTypes.WickedGod:
                case CardTypes.LegendaryDragon: return new HashSet<string>();
                case CardTypes.ZArcFusion: return new HashSet<string>() { CardTypes.Fusion.TypeAbilityString(), Abilities.Effect.ToNiceString() };
                case CardTypes.ZArcFusionSynchro: return new HashSet<string>() { CardTypes.Fusion.TypeAbilityString(), CardTypes.Synchro.TypeAbilityString(), Abilities.Effect.ToNiceString() };
                case CardTypes.ZArcFusionSynchroXyz: return new HashSet<string>() { CardTypes.Fusion.TypeAbilityString(), CardTypes.Synchro.TypeAbilityString(), CardTypes.Xyz.TypeAbilityString(), Abilities.Effect.ToNiceString() };
                case CardTypes.EvoluteConjoint: return new HashSet<string>() { CardTypes.Evolute.TypeAbilityString(), Abilities.Conjoint.ToNiceString(), Abilities.Effect.ToNiceString() };
                default: return new HashSet<string>() { type.TypeAbilityString(), Abilities.Effect.ToNiceString() };
            }
        }
    }
}

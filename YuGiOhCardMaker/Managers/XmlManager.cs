﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Xml.Linq;

namespace YuGiOhCardMaker.Managers
{
    static class XmlManager
    {

        //To Xml

        private static string ToXml(this Brush brush)
        {
            string result = "";
            switch (brush)
            {
                case SolidColorBrush scb: result = $"<solid>{scb.Color}</solid>"; break;
                case LinearGradientBrush lgb: result = $"<gradient angle=\"{lgb.Angle()}\">" + 
                        string.Join("", lgb.GradientStops.Select(gs => $"<stop offset=\"{gs.Offset}\">{gs.Color}</stop>")) + 
                        "</gradient>"; break;
                case ImageBrush ib: result = $"<image>{ib.ImageSource}</image>"; break;
            }

            return result;
        }

        public static string ToXml(this OutlineTextControl outline)
        {
            string result = $"<outline><fill>{outline.Fill.ToXml()}</fill><stroke thickness=\"{outline.StrokeThickness}\">{outline.Stroke.ToXml()}</stroke>";
            if (outline.Paragraph != null)
            {
                foreach (Run i in outline.Paragraph.Inlines.OfType<Run>())
                {
                    bool bold = i.FontWeight == FontWeights.Bold;
                    bool italic = i.FontStyle == FontStyles.Italic;
                    bool underline = i.TextDecorations == TextDecorations.Underline;
                    result += $"<run bold=\"{bold}\" italic=\"{italic}\" underline=\"{underline}\" family=\"{i.FontFamily.FamilyNames.First().Value}\" size=\"{i.FontSize}\">{i.Text}</run>";
                }
            }

            return result + "</outline>";
        }

        public static string ToXml(this TextBox box)
        {
            bool bold = box.FontWeight == FontWeights.Bold;
            bool italic = box.FontStyle == FontStyles.Italic;
            bool underline = box.TextDecorations == TextDecorations.Underline;
            return $"<text bold=\"{bold}\" italic=\"{italic}\" underline=\"{underline}\" family=\"{box.FontFamily.FamilyNames.First().Value}\" " +
                $"size=\"{box.FontSize}\" color=\"{box.Foreground}\">{box.Text}</text>";
        }

        public static string ToXml(this RichTextBox box)
        {
            string result = "<text>";
            foreach (Paragraph b in box.Document.Blocks.OfType<Paragraph>())
            {
                result += $"<p alignment=\"{b.TextAlignment}\">";
                foreach (Run i in b.Inlines.OfType<Run>())
                {
                    bool bold = i.FontWeight == FontWeights.Bold;
                    bool italic = i.FontStyle == FontStyles.Italic;
                    bool underline = i.TextDecorations == TextDecorations.Underline;
                    result += $"<run bold=\"{bold}\" italic=\"{italic}\" underline=\"{underline}\" family=\"{i.FontFamily.FamilyNames.First().Value}\" "+
                        $"size=\"{i.FontSize}\" color=\"{i.Foreground}\">{i.Text}</run>";
                }
                result += "</p>";
            }
            result += "</text>";
            return result;
        }

        //From Xml

        public static void ToRichTextBox(this string text, RichTextBox box)
        {
            try
            {
                box.Document.Blocks.Clear();
            }
            catch { }
            foreach (XElement p in XDocument.Parse(text).Root.Elements())
            {
                Paragraph paragraph = new Paragraph
                {
                    TextAlignment = (TextAlignment)Enum.Parse(typeof(TextAlignment), p.Attribute("alignment").Value)
                };
                foreach (XElement e in p.Elements("run"))
                {
                    Run run = new Run(e.Value)
                    {
                        FontWeight = bool.Parse(e.Attribute("bold").Value) ? FontWeights.Bold : FontWeights.Normal,
                        FontStyle = bool.Parse(e.Attribute("italic").Value) ? FontStyles.Italic : FontStyles.Normal,
                        TextDecorations = bool.Parse(e.Attribute("underline").Value) ? TextDecorations.Underline : null,
                        FontFamily = Managers.Fonts.LoadFont(e.Attribute("family").Value),
                        FontSize = double.Parse(e.Attribute("size").Value),
                        Foreground = e.Attribute("color").Value.ToSolidBrush()
                    };
                    paragraph.Inlines.Add(run);
                }
                box.Document.Blocks.Add(paragraph);
            }
        }

        private static Brush ToBrush(this XElement element)
        {
            GradientStopCollection ToGradientStopCollection(XElement gradient)
            {
                return new GradientStopCollection(gradient.Elements("stop").Select(e => new GradientStop(e.Value.ToColor(), double.Parse(e.Attribute("offset").Value))));
            }

            if (element.Elements("solid").Count() > 0)
                return element.Element("solid").Value.ToSolidBrush();
            else if (element.Elements("gradient").Count() > 0)
                return new LinearGradientBrush(ToGradientStopCollection(element.Element("gradient")), double.Parse(element.Element("gradient").Attribute("angle").Value));
            else if (element.Elements("image").Count() > 0)
                return new ImageBrush(new ImageSourceConverter().ConvertFrom(element.Element("image").Value) as ImageSource);
            else
                return new ImageBrush();
        }

        public static void ToOutline(this string text, OutlineTextControl outline)
        {
            XElement root = XDocument.Parse(text).Root;
            outline.Fill = root.Element("fill").ToBrush();
            outline.Stroke = root.Element("stroke").ToBrush();
            outline.StrokeThickness = ushort.Parse(root.Element("stroke").Attribute("thickness").Value);

            Paragraph paragraph = new Paragraph();
            foreach (XElement r in root.Elements("run"))
            {
                Run run = new Run(r.Value)
                {
                    FontWeight = bool.Parse(r.Attribute("bold").Value) ? FontWeights.Bold : FontWeights.Normal,
                    FontStyle = bool.Parse(r.Attribute("italic").Value) ? FontStyles.Italic : FontStyles.Normal,
                    TextDecorations = bool.Parse(r.Attribute("underline").Value) ? TextDecorations.Underline : null,
                    FontFamily = Managers.Fonts.LoadFont(r.Attribute("family").Value),
                    FontSize = double.Parse(r.Attribute("size").Value)
                };
                paragraph.Inlines.Add(run);
            }
            outline.Paragraph = paragraph;
        }

        public static void ToTextBox(this string text, TextBox box)
        {
            XElement root = XDocument.Parse(text).Root;
            box.Text = root.Value;
            box.FontWeight = bool.Parse(root.Attribute("bold").Value) ? FontWeights.Bold : FontWeights.Normal;
            box.FontStyle = bool.Parse(root.Attribute("italic").Value) ? FontStyles.Italic : FontStyles.Normal;
            box.TextDecorations = bool.Parse(root.Attribute("underline").Value) ? TextDecorations.Underline : null;
            box.FontFamily = Managers.Fonts.LoadFont(root.Attribute("family").Value);
            box.FontSize = double.Parse(root.Attribute("size").Value);
            box.Foreground = root.Attribute("color").Value.ToSolidBrush();
        }
    }
}

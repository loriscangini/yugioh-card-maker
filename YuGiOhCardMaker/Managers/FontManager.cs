﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    class FontManager
    {
        private readonly Card card;
        private readonly ColorManager colorManager;

        public FontManager(Card card)
        {
            this.card = card;
            colorManager = new ColorManager(card);
        }

        public void SetRushLevel(OutlineTextControl levelOutline, TextBox level)
        {
            levelOutline.Paragraph = new Paragraph(new Run(level.Text)
            {
                FontSize = level.FontSize,
                FontFamily = level.FontFamily
            });
        }

        public void SetEffect(Control effect, Control bottom, Control pendulum)
        {
            if (card.CardType == CardTypes.Normal)
            {
                effect.FontFamily = Managers.Fonts.StoneSerif;
                effect.FontStyle = FontStyles.Italic;
                bottom.FontFamily = Managers.Fonts.MatrixBook;
                pendulum.FontFamily = bottom.FontFamily;
            }
            else
            {
                effect.FontFamily = Managers.Fonts.MatrixBook;
                effect.FontStyle = FontStyles.Normal;
                bottom.FontFamily = effect.FontFamily;
                pendulum.FontFamily = effect.FontFamily;
            }
        }

        public void SetATKDEFLink(
            TextBox ATK, Control ATKLabel, 
            TextBox DEF, Control DEFLabel,
            TextBox MaximumATK,
            TextBox link, Control linkLabel,
            TextBox maximumATK, Control maximumATKLabel,
            OutlineTextControl rushATK, OutlineTextControl rushDEF, OutlineTextControl rushMaximumATK)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                    ATK.FontFamily = Fonts.MatrixSmallCaps;
                    ATKLabel.FontFamily = Fonts.MatrixSmallCaps;
                    DEF.FontFamily = Fonts.MatrixSmallCaps;
                    DEFLabel.FontFamily = Fonts.MatrixSmallCaps;
                    maximumATK.FontFamily = Fonts.MatrixSmallCaps;
                    maximumATKLabel.FontFamily = Fonts.MatrixSmallCaps;

                    ATKLabel.FontSize = FontSizes.ATKDEF.Manga;
                    ATK.FontSize = FontSizes.ATKDEF.Manga;
                    DEFLabel.FontSize = FontSizes.ATKDEF.Manga;
                    DEF.FontSize = FontSizes.ATKDEF.Manga;
                    maximumATKLabel.FontSize = FontSizes.ATKDEF.Manga;
                    maximumATK.FontSize = FontSizes.ATKDEF.Manga;

                    ATK.FontWeight = FontWeights.SemiBold;
                    DEF.FontWeight = FontWeights.SemiBold;
                    link.FontWeight = FontWeights.SemiBold;
                    maximumATK.FontWeight = FontWeights.SemiBold;
                    break;
                case Series.Rush:
                    ATK.FontFamily = Fonts.RoGSanSerifStdB;
                    DEF.FontFamily = Fonts.RoGSanSerifStdB;
                    link.FontFamily = Fonts.RoGSanSerifStdB;
                    maximumATK.FontFamily = Fonts.RoGSanSerifStdB;

                    ATK.FontSize = FontSizes.ATKDEF.Rush;
                    DEF.FontSize = FontSizes.ATKDEF.Rush;
                    link.FontSize = FontSizes.Link.Rush;
                    maximumATK.FontSize = FontSizes.Link.Rush;

                    ATK.FontWeight = FontWeights.SemiBold;
                    DEF.FontWeight = FontWeights.SemiBold;
                    link.FontWeight = FontWeights.SemiBold;
                    maximumATK.FontWeight = FontWeights.SemiBold;
                    break;
                case Series.ForKids:
                    ATK.FontFamily = Fonts.ITCSouvenir;
                    DEF.FontFamily = Fonts.ITCSouvenir;
                    link.FontFamily = Fonts.ITCSouvenir;
                    linkLabel.FontFamily = Fonts.ITCSouvenir;

                    ATK.FontSize = FontSizes.ATKDEF.Kids;
                    DEF.FontSize = FontSizes.ATKDEF.Kids;
                    link.FontSize = FontSizes.ATKDEF.KidsLink;
                    linkLabel.FontSize = FontSizes.ATKDEF.KidsLink;

                    ATK.FontWeight = FontWeights.Normal;
                    DEF.FontWeight = FontWeights.Normal;
                    link.FontWeight = FontWeights.Normal;
                    linkLabel.FontWeight = FontWeights.Normal;
                    break;
                default:
                    ATK.FontFamily = Fonts.MatrixSmallCaps;
                    ATKLabel.FontFamily = Fonts.MatrixSmallCaps;
                    DEF.FontFamily = Fonts.MatrixSmallCaps;
                    DEFLabel.FontFamily = Fonts.MatrixSmallCaps;
                    link.FontFamily = Fonts.RoGSanSerifStdB;
                    linkLabel.FontFamily = Fonts.EurostyleCandy;
                    maximumATK.FontFamily = Fonts.MatrixSmallCaps;
                    maximumATKLabel.FontFamily = Fonts.MatrixSmallCaps;

                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^\?+$");
                    ATKLabel.FontSize = FontSizes.ATKDEF.Normal;
                    ATK.FontSize = regex.IsMatch(ATK.Text) ? FontSizes.ATKDEF.QMark : FontSizes.ATKDEF.Normal;
                    DEFLabel.FontSize = FontSizes.ATKDEF.Normal;
                    DEF.FontSize = regex.IsMatch(DEF.Text) ? FontSizes.ATKDEF.QMark : FontSizes.ATKDEF.Normal;
                    link.FontSize = FontSizes.Link.Normal;
                    linkLabel.FontSize = FontSizes.Link.LabelNormal;
                    maximumATK.FontSize = regex.IsMatch(ATK.Text) ? FontSizes.ATKDEF.QMark : FontSizes.ATKDEF.Normal;
                    maximumATKLabel.FontSize = FontSizes.ATKDEF.Normal;

                    ATK.FontWeight = FontWeights.SemiBold;
                    DEF.FontWeight = FontWeights.SemiBold;
                    link.FontWeight = FontWeights.SemiBold;
                    linkLabel.FontWeight = FontWeights.SemiBold;
                    maximumATKLabel.FontWeight = FontWeights.SemiBold;
                    break;
            }

            colorManager.SetATKDEFLink(ATK, DEF, link, MaximumATK);

            rushATK.Paragraph = new Paragraph(new Run(ATK.Text) { FontFamily = ATK.FontFamily, FontSize = ATK.FontSize });
            rushDEF.Paragraph = new Paragraph(new Run(DEF.Text) { FontFamily = DEF.FontFamily, FontSize = DEF.FontSize });
            rushMaximumATK.Paragraph = new Paragraph(new Run(MaximumATK.Text) { FontFamily = MaximumATK.FontFamily, FontSize = MaximumATK.FontSize });
        }
    }

    public static class Fonts
    {
        public static FontFamily LoadFont(string fontName) 
        {
            FontFamily font = new FontFamily(fontName);
            return System.Windows.Media.Fonts.SystemFontFamilies.Contains(font) ? font : InternalFont(fontName);
        }

        private static FontFamily InternalFont(string fontName) => 
            new FontFamily(new Uri("pack://application:,,,/"), $"./res/fonts/#{fontName}");

        public static FontFamily DFLeisho1 = InternalFont("Yu-Gi-Oh! DF Leisho 1");
        public static FontFamily DFLeisho2 = InternalFont("Yu-Gi-Oh! DF Leisho 2");
        public static FontFamily DFLeisho3 = InternalFont("Yu-Gi-Oh! DF Leisho 3");
        public static FontFamily DFLeisho4 = InternalFont("Yu-Gi-Oh! DF Leisho 4");
        public static FontFamily EurostyleCandy = InternalFont("EurostileCandyW01");
        public static FontFamily MatrixBook = InternalFont("MatrixBook");
        public static FontFamily MatrixRegularSmallCaps = InternalFont("MatrixRegularSmallCaps");
        public static FontFamily MatrixSmallCaps = InternalFont("MatrixSmallCaps");
        public static FontFamily RoGSanSerifStdB = InternalFont("Ro GSan Serif Std B");
        public static FontFamily SanDiegoStAztecs = InternalFont("NCAA San Diego St Aztecs");
        public static FontFamily StoneSerif = InternalFont("Stone Serif");
        public static FontFamily StoneSerifSC = InternalFont("Stone Serif SC ITC TT");
        public static FontFamily YuGothicUI = InternalFont("Yu Gothic UI Semibold");
        public static FontFamily GillSansMTProHeavy = InternalFont("Gill Sans MT Pro Heavy");
        public static FontFamily ITCSouvenir = InternalFont("ITC Souvenir");

        public static IEnumerable<FontFamily> CustomFonts = new List<FontFamily>()
        {
            DFLeisho1, DFLeisho2, DFLeisho3, DFLeisho4, EurostyleCandy, MatrixBook, MatrixRegularSmallCaps, ITCSouvenir,
            MatrixSmallCaps, RoGSanSerifStdB, SanDiegoStAztecs, StoneSerif, StoneSerifSC, YuGothicUI, GillSansMTProHeavy
        };
    }

    public static class FontSizes
    {
        public static class ATKDEF
        {
            public static int Normal = 25;
            public static int QMark = 30;
            public static int Rush = 25;
            public static int Manga = 45;
            public static int Kids = 65;
            public static int KidsLink = 50;
        }
        public static class Link
        {
            public static int Normal = 20;
            public static int Rush = 25;
            public static int LabelNormal = 22;
        }
    }
}

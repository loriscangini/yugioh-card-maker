﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    public class VisibilityManager
    {
        private readonly Card card;
        private readonly ImageManager imageManager;

        public VisibilityManager(Card card)
        {
            this.card = card;
            imageManager = new ImageManager(card);
        }

        public void SetName(UIElement name) => name.Visibility = card.Series.In(Series.ForKids) ? Visibility.Hidden : Visibility.Visible;
        public void SetNameOutline(UIElement nameOutline, bool useGradient)
        {
            nameOutline.Visibility = useGradient ? Visibility.Visible : Visibility.Hidden;
        }

        public void SetAttribute(UIElement attribute)
        {
            if (!card.HasAttribute)
                attribute.Visibility = Visibility.Hidden;
            else switch (card.Series)
                {
                    case Series.Manga when card.IsOriginalManga:
                        attribute.Visibility = Visibility.Hidden;
                        break;
                    default:
                        attribute.Visibility = Visibility.Visible;
                        break;
                }
        }

        public void SetCustom(params UIElement[] elements) {
            foreach (UIElement item in elements)
            {
                item.Visibility = card.IsCustom ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public void SetKingHierarchy(UIElement hierarchy) => hierarchy.Visibility = card.CardType == CardTypes.King ? Visibility.Visible : Visibility.Hidden;

        public void SetStars(Grid stars, UIElement evolute1, UIElement evolute2, UIElement vigor, UIElement rushLevel, string path = null)
        {
            stars.Visibility = Visibility.Visible;
            evolute1.Visibility = Visibility.Hidden;
            evolute2.Visibility = Visibility.Hidden;
            vigor.Visibility = Visibility.Hidden;
            rushLevel.Visibility = Visibility.Hidden;

            if (card.Series == Series.Rush)
            {
                stars.Visibility = Visibility.Hidden;
                rushLevel.Visibility = card.StarType != StarTypes.None ? Visibility.Visible : Visibility.Hidden;
            }
            else switch (card.StarType)
                {
                    case StarTypes.Evolute:
                        stars.Visibility = Visibility.Hidden;
                        evolute1.Visibility = Visibility.Visible;
                        if (card.CardType == CardTypes.EvoluteConjoint)
                            evolute2.Visibility = Visibility.Visible;
                        break;
                    case StarTypes.Vigor:
                        stars.Visibility = Visibility.Hidden;
                        vigor.Visibility = Visibility.Visible;
                        break;
                    case StarTypes.None:
                        stars.Visibility = Visibility.Hidden;
                        break;
                }

            imageManager.SetStars(stars, card.IsCustom ? path : null);
        }
        public int GetStarValue(Image star) => card.HasRightAlignedStars ? (13 - Grid.GetColumn(star)) : (Grid.GetColumn(star) + 1);
        public void SetStarsOpacity(Grid stars)
        {
            stars.Children.OfType<Image>().ToList().ForEach(s => s.Opacity = GetStarValue(s) > card.Stars ? 0.25 : 1);
        }
        public void HideStars(Grid stars)
        {
            stars.Children.OfType<Image>().ToList().ForEach(s =>
            {
                if (GetStarValue(s) > card.Stars)
                {
                    s.Visibility = !card.HasCenteredStars ? Visibility.Hidden : Visibility.Collapsed;
                    if (GetStarValue(s) == 13)
                        s.Visibility = Visibility.Collapsed;
                }
                else
                    s.Visibility = Visibility.Visible;
            });
        }

        public void SetLegend(UIElement legend) => legend.Visibility = card.IsLegend ? Visibility.Visible : Visibility.Hidden;
        public void SetFoil(UIElement foil) => foil.Visibility = card.Foil == Foil.None ? Visibility.Hidden : Visibility.Visible;
        public void SetLinkArrows(UIElement arrows) => arrows.Visibility = card.HasLinkArrows ? Visibility.Visible : Visibility.Hidden;
        public void SetCriplify(UIElement criplifyFactor) => criplifyFactor.Visibility = card.CardType == CardTypes.Criplify ? Visibility.Visible : Visibility.Hidden;
        public void SetCardNumber(UIElement cardNumber)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                case Series.ForKids:
                    cardNumber.Visibility = Visibility.Hidden;
                    break;
                default:
                    cardNumber.Visibility = Visibility.Visible;
                    break;
            }
        }

        public void SetPendulumDeckMaster(UIElement pendulum, UIElement leftScale, UIElement rightScale, UIElement pendulumEffect, UIElement pendulumBar)
        {
            if (card.IsPendulum)
            {
                pendulum.Visibility = Visibility.Visible;
                pendulumEffect.Visibility = Visibility.Visible;
                pendulumBar.Visibility = card.IsFullImage ? Visibility.Hidden : Visibility.Visible;
                leftScale.Visibility = Visibility.Visible;
                rightScale.Visibility = Visibility.Visible;
            }
            else if (card.IsDeckMaster)
            {
                pendulum.Visibility = Visibility.Visible;
                pendulumEffect.Visibility = Visibility.Visible;
                pendulumBar.Visibility = card.IsFullImage ? Visibility.Hidden : Visibility.Visible;
                leftScale.Visibility = Visibility.Hidden;
                rightScale.Visibility = Visibility.Hidden;
            }
            else
            {
                pendulum.Visibility = Visibility.Hidden;
                pendulumEffect.Visibility = Visibility.Hidden;
                pendulumBar.Visibility = Visibility.Hidden;
                leftScale.Visibility = Visibility.Hidden;
                rightScale.Visibility = Visibility.Hidden;
            }
        }
        public void SetTypeAbilityLine(UIElement typeAbility)
        {
            switch (card.Series)
            {
                case Series.ForKids:
                    typeAbility.Visibility = Visibility.Hidden;
                    break;
                case Series.Rush:
                    typeAbility.Visibility = Visibility.Visible;
                    break;
                default:
                    typeAbility.Visibility = card.HasTypeAbilityText || card.HasTopType ? Visibility.Visible : Visibility.Collapsed;
                    break;
            }
        }
        public void SetEffect(UIElement typeAbility)
        {
            switch (card.Series)
            {
                case Series.ForKids:
                    typeAbility.Visibility = Visibility.Hidden;
                    break;
                default:
                    typeAbility.Visibility = Visibility.Visible;
                    break;
            }
        }

        internal void SetOutline(OutlineTextControl outline, bool showOutline)
        {
            if (outline != null)
            {
                outline.Visibility = showOutline ? Visibility.Visible : Visibility.Hidden;
            }
        }

        public void SetSpeedDuel(UIElement speedOverlay) => speedOverlay.Visibility = card.IsSpeedDuel ? Visibility.Visible : Visibility.Hidden;
        public void SetSpeedSpell(UIElement speedSpellOverlay) => speedSpellOverlay.Visibility = card.IsSpeedSpell ? Visibility.Visible : Visibility.Hidden;
        public void SetBottomEffect(System.Windows.Controls.RichTextBox element)
        {
            if (card.HasBottomEffect && card.Series != Series.ForKids)
            {
                element.Visibility = Visibility.Visible;
                element.Document.Blocks.Clear();
                if (card.CardType == CardTypes.Token)
                    element.Document.Blocks.Add(new Paragraph(new Run("*If used for another Token, apply that Token's Type/Attribute/Level/ATK/DEF.")));
            }
            else
                element.Visibility = Visibility.Hidden;
        }
        public void SetATKDEFLink(
            UIElement ATK, UIElement ATKLabel, UIElement RushATK,
            UIElement DEF, UIElement DEFLabel, UIElement RushDEF,
            UIElement linkNumber, UIElement linkLabel,
            UIElement maximumATKLayout, UIElement maximumATK, UIElement maximumATKLabel, UIElement RushMaximumATK
        )
        {
            bool isRush = card.Series == Series.Rush;
            bool isKids = card.Series == Series.ForKids;

            if (card.IsMonster && !card.CardType.In(CardTypes.Phasm))
            {
                ATK.Visibility = card.HasATK ? Visibility.Visible : Visibility.Hidden;
                ATKLabel.Visibility = !isRush && !isKids ? ATK.Visibility : Visibility.Hidden;
                RushATK.Visibility = isRush ? ATK.Visibility : Visibility.Hidden;
                if (card.HasLinkArrows)
                {
                    DEF.Visibility = Visibility.Hidden;
                    DEFLabel.Visibility = Visibility.Hidden;
                    linkNumber.Visibility = Visibility.Visible;
                    linkLabel.Visibility = !isRush ? Visibility.Visible : Visibility.Hidden;
                }
                else
                {
                    DEF.Visibility = card.HasDEF ? Visibility.Visible : Visibility.Hidden;
                    DEFLabel.Visibility = !isRush && !isKids ? DEF.Visibility : Visibility.Hidden;
                    RushDEF.Visibility = isRush && !isKids ? DEF.Visibility : Visibility.Hidden;
                    linkNumber.Visibility = Visibility.Hidden;
                    linkLabel.Visibility = Visibility.Hidden;
                }

                maximumATKLayout.Visibility = card.IsMaximum && isRush ? Visibility.Visible : Visibility.Hidden;
                maximumATK.Visibility = card.IsMaximum ? Visibility.Visible : Visibility.Hidden;
                maximumATKLabel.Visibility = card.IsMaximum ? Visibility.Visible : Visibility.Hidden;
                RushMaximumATK.Visibility = card.IsMaximum && isRush ? Visibility.Visible : Visibility.Hidden;
            }
            else
            {
                ATK.Visibility = Visibility.Hidden;
                ATKLabel.Visibility = Visibility.Hidden;
                RushATK.Visibility = Visibility.Hidden;
                DEF.Visibility = Visibility.Hidden;
                DEFLabel.Visibility = Visibility.Hidden;
                RushDEF.Visibility = Visibility.Hidden;
                linkNumber.Visibility = Visibility.Hidden;
                linkLabel.Visibility = Visibility.Hidden;
                maximumATKLayout.Visibility = Visibility.Hidden;
                maximumATK.Visibility = Visibility.Hidden;
                maximumATKLabel.Visibility = Visibility.Hidden;
                RushMaximumATK.Visibility = Visibility.Hidden;
            }
        }

        public void SetPasscode(UIElement passcode)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                    passcode.Visibility = Visibility.Hidden;
                    break;
                default:
                    passcode.Visibility = Visibility.Visible;
                    break;
            }
        }
        public void SetEdition(UIElement first, UIElement limited, UIElement terminal, Image sticker)
        {
            switch (card.Edition)
            {
                case Editions.Unlimited:
                    first.Visibility = Visibility.Hidden;
                    limited.Visibility = Visibility.Hidden;
                    terminal.Visibility = Visibility.Hidden;
                    sticker.Visibility = Visibility.Visible;
                    break;
                case Editions.Limited:
                    first.Visibility = Visibility.Hidden;
                    limited.Visibility = Visibility.Visible;
                    terminal.Visibility = Visibility.Hidden;
                    sticker.Visibility = Visibility.Visible;
                    break;
                case Editions.First:
                    first.Visibility = Visibility.Visible;
                    limited.Visibility = Visibility.Hidden;
                    terminal.Visibility = Visibility.Hidden;
                    sticker.Visibility = Visibility.Visible;
                    break;
                case Editions.Terminal:
                    first.Visibility = Visibility.Hidden;
                    limited.Visibility = Visibility.Hidden;
                    terminal.Visibility = Visibility.Visible;
                    sticker.Visibility = Visibility.Visible;
                    break;
                default:
                    first.Visibility = Visibility.Hidden;
                    limited.Visibility = Visibility.Hidden;
                    terminal.Visibility = Visibility.Hidden;
                    sticker.Visibility = Visibility.Hidden;
                    break;
            }
            imageManager.SetSticker(sticker);

            if (card.Series == Series.ForKids)
            {
                first.Visibility = Visibility.Hidden;
                limited.Visibility = Visibility.Hidden;
                terminal.Visibility = Visibility.Hidden;
            }
        }
        public void SetCopyright(UIElement copyright)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                case Series.Rush:
                case Series.ForKids:
                    copyright.Visibility = Visibility.Hidden;
                    break;
                default:
                    copyright.Visibility = Visibility.Visible;
                    break;
            }
        }
        public void SetGoldPlatinum(UIElement parts)
        {
            if (card.IsGoldRare)
            {
                parts.Visibility = Visibility.Visible;
            }
            else if (card.IsPlatinumRare)
            {
                parts.Visibility = Visibility.Visible;
            }
            else
            {
                parts.Visibility = Visibility.Hidden;
            }
        }
        public void SetBorder(UIElement border)
        {
            switch (card.Series)
            {
                case Series.ForKids:
                    border.Visibility = Visibility.Visible;
                    break;
                default:
                    border.Visibility = Visibility.Visible;
                    break;
            }
        }
    }
}

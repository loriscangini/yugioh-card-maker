﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace YuGiOhCardMaker.Managers
{
    class Manager
    {
        public VisibilityManager Visibility { get; }
        public PositionManager Position { get; }
        public ImageManager Image { get; }
        public EnabledManager Enabled { get; }
        public FontManager Font { get; }
        public ColorManager Color { get; }

        public Manager(Card card)
        {
            Visibility = new VisibilityManager(card);
            Position = new PositionManager(card);
            Image = new ImageManager(card);
            Enabled = new EnabledManager(card);
            Font = new FontManager(card);
            Color = new ColorManager(card);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    class ImageManager
    {
        private readonly Card card;

        public ImageManager(Card card) {
            this.card = card;
        }

        public void SetFrame(Image frame, string path = null)
        {
            if (string.IsNullOrEmpty(path))
                frame.Source = Images.BitmapRes($"cardtype/{card.Series}/{card.CardType}", card.IsFullImage);
            else
                frame.Source = new BitmapImage(new Uri(path));
        }
        public void SetAttribute(Image attribute, string customAttribute = null)
        {
            attribute.Source = card.Attribute == Attributes.Custom ? new BitmapImage(new Uri(customAttribute)) : card.Attribute.GetImage(card.Series);
        }

        public void SetBorder(Image border)
        {
            if (card.IsGoldRare)
                border.Source = Images.Border.Gold;
            else if (card.IsPlatinumRare)
                border.Source = Images.Border.Platinum;
            else
                border.Source = Images.Border.Normal(card);
        }
        public void SetLegend(Image legend) => legend.Source = card.IsMonster ? Images.Legend.Gold : Images.Legend.Silver;

        public void SetStars(Grid stars, string path = null)
        {
            int GetStarValue(Image star) => card.HasRightAlignedStars ? (13 - Grid.GetColumn(star)) : (Grid.GetColumn(star) + 1);

            void SetStarType(ImageSource starType) => stars.Children.OfType<Image>().ToList().ForEach(s => s.Source = starType);

            if (path == null)
            {
                switch (card.StarType)
                {
                    case StarTypes.Level:
                        SetStarType(Images.Star.Level(card.Series));
                        if (card.CardType.In(CardTypes.Evolution))
                            stars.Children.OfType<Image>().ToList().First(s => GetStarValue(s) == card.Stars).Source = Images.Star.EvoStar;
                        break;
                    case StarTypes.Rank:
                        SetStarType(Images.Star.Rank(card.Series));
                        break;
                    case StarTypes.NegativeLevel:
                        SetStarType(Images.Star.NegativeLevel(card.Series));
                        break;
                }
            }
            else {
                SetStarType(new BitmapImage(new Uri(path)));
            }
        }

        public void SetFoil(Image foil) => foil.Source = Images.BitmapRes($"foils/{card.Foil}foil");

        public void SetGoldPlatinum(Image parts)
        {
            if (card.IsGoldRare)
            {
                if (card.IsPendulum)
                {
                    switch (card.ActiveBoxSize)
                    {
                        case BoxSize.Small: parts.Source = Images.Foil.Gold.PendulumSmall(card.IsFullImage); break;
                        case BoxSize.Medium: parts.Source = Images.Foil.Gold.PendulumMedium(card.IsFullImage); break;
                        case BoxSize.Large: parts.Source = Images.Foil.Gold.PendulumLarge(card.IsFullImage); break;
                    }
                }
                else if (card.IsDeckMaster)
                {
                    switch (card.ActiveBoxSize)
                    {
                        case BoxSize.Small: parts.Source = Images.Foil.Gold.DeckMasterSmall(card.IsFullImage); break;
                        case BoxSize.Medium: parts.Source = Images.Foil.Gold.DeckMasterMedium(card.IsFullImage); break;
                        case BoxSize.Large: parts.Source = Images.Foil.Gold.DeckMasterLarge(card.IsFullImage); break;
                    }
                }
                else
                    parts.Source = Images.Foil.Gold.NonPendulum(card.IsFullImage);
            }
            else if (card.IsPlatinumRare)
            {
                if (card.IsPendulum)
                {
                    switch (card.ActiveBoxSize)
                    {
                        case BoxSize.Small: parts.Source = Images.Foil.Platinum.PendulumSmall(card.IsFullImage); break;
                        case BoxSize.Medium: parts.Source = Images.Foil.Platinum.PendulumMedium(card.IsFullImage); break;
                        case BoxSize.Large: parts.Source = Images.Foil.Platinum.PendulumLarge(card.IsFullImage); break;
                    }
                }
                else if (card.IsDeckMaster)
                {
                    switch (card.ActiveBoxSize)
                    {
                        case BoxSize.Small: parts.Source = Images.Foil.Platinum.DeckMasterSmall(card.IsFullImage); break;
                        case BoxSize.Medium: parts.Source = Images.Foil.Platinum.DeckMasterMedium(card.IsFullImage); break;
                        case BoxSize.Large: parts.Source = Images.Foil.Platinum.DeckMasterLarge(card.IsFullImage); break;
                    }
                }
                else
                    parts.Source = Images.Foil.Platinum.NonPendulum(card.IsFullImage);
            }
        }

        public void SetAttributes(Image dark, Image divine, Image earth, Image fire, Image light, Image water, Image wind, Image laugh,
            Image spell, Image trap, Image trapSpell, Image virus, Image king, Image attribute)
        {
            dark.Source = Attributes.Dark.GetImage(card.Series);
            divine.Source = Attributes.Divine.GetImage(card.Series);
            earth.Source = Attributes.Earth.GetImage(card.Series);
            fire.Source = Attributes.Fire.GetImage(card.Series);
            light.Source = Attributes.Light.GetImage(card.Series);
            water.Source = Attributes.Water.GetImage(card.Series);
            wind.Source = Attributes.Wind.GetImage(card.Series);
            laugh.Source = Attributes.Laugh.GetImage(card.Series);
            spell.Source = Attributes.Spell.GetImage(card.Series);
            trap.Source = Attributes.Trap.GetImage(card.Series);
            trapSpell.Source = Attributes.TrapSpell.GetImage(card.Series);
            virus.Source = Attributes.Virus.GetImage(card.Series);
            king.Source = Attributes.King.GetImage(card.Series);

            attribute.Source = card.Attribute.GetImage(card.Series);
        }

        public void SetSymbols(Image continuous, Image counter, Image equip, Image field, Image quick, Image ritual)
        {
            continuous.Source = Symbols.Continuous.GetImage(card.Series);
            counter.Source = Symbols.Counter.GetImage(card.Series);
            equip.Source = Symbols.Equip.GetImage(card.Series);
            field.Source = Symbols.Field.GetImage(card.Series);
            quick.Source = Symbols.QuickPlay.GetImage(card.Series);
            ritual.Source = Symbols.Ritual.GetImage(card.Series);
        }

        public void SetPendulumDeckMaster(Image pendulumFrame, Image pendulumBar)
        {
            if (card.IsPendulum)
            {
                switch (card.ActiveBoxSize)
                {
                    case BoxSize.Small:
                        pendulumFrame.Source = Images.PendulumBox.Small(card.Series, card.IsFullImage);
                        break;
                    case BoxSize.Medium:
                        pendulumFrame.Source = Images.PendulumBox.Medium(card.Series, card.IsFullImage);
                        break;
                    case BoxSize.Large:
                        pendulumFrame.Source = Images.PendulumBox.Large(card.Series, card.IsFullImage);
                        break;
                }
            }
            if (card.IsDeckMaster)
            {
                switch (card.ActiveBoxSize)
                {
                    case BoxSize.Small:
                        pendulumFrame.Source = Images.DeckMasterBox.Small(card.Series, card.IsFullImage);
                        break;
                    case BoxSize.Medium:
                    case BoxSize.Large:
                        pendulumFrame.Source = Images.DeckMasterBox.Medium(card.Series, card.IsFullImage);
                        break;
                }
            }
            pendulumBar.Source = Images.BitmapRes($"cardtype/{card.Series}/{card.CardType}_pendulum");
        }

        public void SetLinkArrows(Image topLeft, Image top, Image topRight, Image left, Image right, Image bottomLeft, Image bottom, Image bottomRight)
        {
            topLeft.Source = Images.LinkArrows.TopLeft(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[0]);
            top.Source = Images.LinkArrows.Top(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[1]);
            topRight.Source = Images.LinkArrows.TopRight(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[2]);
            left.Source = Images.LinkArrows.Left(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[3]);
            right.Source = Images.LinkArrows.Right(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[4]);
            bottomLeft.Source = Images.LinkArrows.BottomLeft(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[5]);
            bottom.Source = Images.LinkArrows.Bottom(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[6]);
            bottomRight.Source = Images.LinkArrows.BottomRight(card.IsGoldRare, card.IsPlatinumRare, card.LinkArrows[7]);
        }

        public void SetSticker(Image sticker)
        {
            switch (card.Edition)
            {
                case Editions.Unlimited:
                    sticker.Source = Images.Sticker.Silver;
                    break;
                case Editions.Limited:
                case Editions.First:
                    sticker.Source = Images.Sticker.Gold;
                    break;
            }
        }
    }

    static class Images
    {
        private static Uri Res(string toLoad) => new Uri($"/YuGiOhCardMaker;component/res/{toLoad}", UriKind.Relative);
        public static ImageSource BitmapRes(string toLoad, bool full = false) => new BitmapImage(Res(toLoad + (full ? "_full" : "") + ".png"));

        public static ImageSource GetImage(this Attributes attribute, string series) => BitmapRes($"cardtype/{series}/attributes/{attribute}");
        public static ImageSource GetImage(this Symbols symbol, string series) => BitmapRes($"symbols/{(series == Series.Manga ? "manga/" : "")}{symbol}");

        public static class Star
        {
            public static ImageSource Level(string series) => BitmapRes($"stars/{(series == Series.Manga ? "manga-" : "")}level");
            public static ImageSource Rank(string series) => BitmapRes($"stars/{(series == Series.Manga ? "manga-" : "")}rank");
            public static ImageSource NegativeLevel(string series) => BitmapRes($"stars/{(series == Series.Manga ? "manga-" : "")}negLevel");
            public static readonly ImageSource EvoStar = BitmapRes("stars/evostar");
        }

        public static class PendulumBox
        {
            public static ImageSource Small(string series, bool isFull) => BitmapRes($"cardtype/{series}/pendulum_S", isFull);
            public static ImageSource Medium(string series, bool isFull) => BitmapRes($"cardtype/{series}/pendulum_M", isFull);
            public static ImageSource Large(string series, bool isFull) => BitmapRes($"cardtype/{series}/pendulum_L", isFull);
        }
        public static class DeckMasterBox
        {
            public static ImageSource Small(string series, bool isFull) => BitmapRes($"cardtype/{series}/master_S", isFull);
            public static ImageSource Medium(string series, bool isFull) => BitmapRes($"cardtype/{series}/master_M", isFull);
        }
        public static class Sticker
        {
            public static readonly ImageSource Silver = BitmapRes("stickerSilver");
            public static readonly ImageSource Gold = BitmapRes("stickerGold");
        }

        public static class Border
        {
            public static ImageSource Normal(Card card)
            {
                switch (card.Series) 
                {
                    case Series.Manga: return BitmapRes($"borders/manga");
                    case Series.ForKids: return BitmapRes($"borders/4kids");
                    default: return BitmapRes($"borders/normal");
                }
            }

            public static readonly ImageSource Gold = BitmapRes("foils/gold/border_gold");
            public static readonly ImageSource Platinum = BitmapRes("foils/platinum/border_platinum");
        }
        public static class Legend
        {
            public static readonly ImageSource Silver = BitmapRes("icons/LegendSilver");
            public static readonly ImageSource Gold = BitmapRes("icons/LegendGold");
        }


        public static class Foil
        {
            private static ImageSource GetImage(string name, string foil, bool full) => BitmapRes($"foils/{foil}/{name}_{foil}" + (full ? "_full" : ""));

            public static class Gold
            {
                public static ImageSource NonPendulum(bool full) => GetImage("normal", "gold", full);
                public static ImageSource PendulumSmall(bool full) => GetImage("pendulum_S", "gold", full);
                public static ImageSource PendulumMedium(bool full) => GetImage("pendulum_M", "gold", full);
                public static ImageSource PendulumLarge(bool full) => GetImage("pendulum_L", "gold", full);
                public static ImageSource DeckMasterSmall(bool full) => GetImage("master_S", "gold", full);
                public static ImageSource DeckMasterMedium(bool full) => GetImage("master_M", "gold", full);
                public static ImageSource DeckMasterLarge(bool full) => GetImage("master_M", "gold", full);
            }
            public static class Platinum
            {
                public static ImageSource NonPendulum(bool full) => GetImage("normal", "platinum", full);
                public static ImageSource PendulumSmall(bool full) => GetImage("pendulum_S", "platinum", full);
                public static ImageSource PendulumMedium(bool full) => GetImage("pendulum_M", "platinum", full);
                public static ImageSource PendulumLarge(bool full) => GetImage("pendulum_L", "platinum", full);
                public static ImageSource DeckMasterSmall(bool full) => GetImage("master_S", "platinum", full);
                public static ImageSource DeckMasterMedium(bool full) => GetImage("master_M", "platinum", full);
                public static ImageSource DeckMasterLarge(bool full) => GetImage("master_M", "platinum", full);
            }
        }

        public static class LinkArrows
        {
            private static string Extra(bool gold, bool platinum, bool active) => (gold ? "_gold" : "") + (platinum ? "_platinum" : "") + (active ? "_active" : "");

            public static ImageSource TopLeft(bool gold, bool platinum, bool active) => BitmapRes($"arrows/topleft{ Extra(gold, platinum, active) }");
            public static ImageSource Top(bool gold, bool platinum, bool active) => BitmapRes($"arrows/top{ Extra(gold, platinum, active)}");
            public static ImageSource TopRight(bool gold, bool platinum, bool active) => BitmapRes($"arrows/topright{ Extra(gold, platinum, active)}");
            public static ImageSource Left(bool gold, bool platinum, bool active) => BitmapRes($"arrows/left{ Extra(gold, platinum, active)}");
            public static ImageSource Right(bool gold, bool platinum, bool active) => BitmapRes($"arrows/right{ Extra(gold, platinum, active)}");
            public static ImageSource BottomLeft(bool gold, bool platinum, bool active) => BitmapRes($"arrows/bottomleft{ Extra(gold, platinum, active)}");
            public static ImageSource Bottom(bool gold, bool platinum, bool active) => BitmapRes($"arrows/bottom{ Extra(gold, platinum, active)}");
            public static ImageSource BottomRight(bool gold, bool platinum, bool active) => BitmapRes($"arrows/bottomright{ Extra(gold, platinum, active)}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    public class ColorManager
    {
        private readonly Card card;

        public ColorManager(Card card)
        {
            this.card = card;
        }

        internal void SetName(Control name)
        {
             name.Foreground = card.HasWhiteName && card.Series != Series.Manga ? Brushes.White : Brushes.Black;
        }

        internal void SetOutline(OutlineTextControl outline, Brush fill, Color stroke, double strokeWidth)
        {
            if (outline != null)
            {
                outline.Fill = fill;
                outline.Stroke = new SolidColorBrush(stroke);
                outline.StrokeThickness = (ushort)strokeWidth;
            }
        }

        internal void Set(Control box, Color color)
        {
            if (box != null)
                box.Foreground = new SolidColorBrush(color);
        }

        public void SetRushLevel(OutlineTextControl rushLevel) 
        {
            Brush fromHex(string hex) => (SolidColorBrush)new BrushConverter().ConvertFrom(hex);

            if (card.Series == Series.Rush)
            {
                switch (card.CardType)
                {
                    case CardTypes.DarkSynchro: rushLevel.Stroke = fromHex("#1d21c8"); break;
                    case CardTypes.Xyz: rushLevel.Stroke = Brushes.Black; break;
                    default: rushLevel.Stroke = fromHex("#c8211d"); break;
                }
            }
        }

        public void SetTypeAbility(Control typeAbility)
        {
            typeAbility.Foreground = card.Series == Series.Rush && card.HasDarkBackColor ? Brushes.White : Brushes.Black;
        }

        public void SetEffect(Control effect, Control bottom)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                    effect.Foreground = card.HasDarkBackColor ? Brushes.White : Brushes.Black;
                    break;
                default:
                    effect.Foreground = Brushes.Black;
                    break;
            }
            bottom.Foreground = effect.Foreground;
        }
        public void SetATKDEFLink(Control ATK, Control DEF, Control link, Control maximumATK)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                    Brush color = card.HasDarkBackColor ? Brushes.White : Brushes.Black;

                    ATK.Foreground = color;
                    DEF.Foreground = color;
                    break;
                case Series.Rush:
                    ATK.Foreground = Brushes.Transparent;
                    DEF.Foreground = Brushes.Transparent;
                    link.Foreground = Brushes.Transparent;
                    maximumATK.Foreground = Brushes.Transparent;
                    break;
                default:
                    if(ATK.Foreground == Brushes.White || ATK.Foreground == Brushes.Transparent)
                        ATK.Foreground = Brushes.Black;
                    if (DEF.Foreground == Brushes.White || DEF.Foreground == Brushes.Transparent)
                        DEF.Foreground = Brushes.Black;
                    if (link.Foreground == Brushes.White || link.Foreground == Brushes.Transparent)
                        link.Foreground = Brushes.Black;
                    if (maximumATK.Foreground == Brushes.White || maximumATK.Foreground == Brushes.Transparent)
                        maximumATK.Foreground = Brushes.Black;
                    break;
            }
        }

        public void SetCardNumber(Control cardNumber)
        {
            switch (card.Series)
            {
                case Series.Rush:
                    cardNumber.Foreground = Brushes.White;
                    break;
                default:
                    cardNumber.Foreground = card.IsPendulum || card.IsDeckMaster || card.IsFullImage || !card.HasDarkBackColor ? Brushes.Black : Brushes.White;
                    break;
            }
        }

        public void SetPasscode(Control passcode)
        {
            passcode.Foreground = card.HasDarkBackColor || card.Series == Series.Rush ? Brushes.White : Brushes.Black;
        }

        public void SetEditions(TextBlock limited, TextBlock first, TextBlock terminal)
        {
            limited.Foreground = card.HasDarkBackColor || card.Series == Series.Rush ? Brushes.White : Brushes.Black;
            first.Foreground = limited.Foreground;
            terminal.Foreground = limited.Foreground;
        }
        public void SetCopyright(Control copyright)
        {
            copyright.Foreground = card.HasDarkBackColor || card.Series == Series.Rush ? Brushes.White : Brushes.Black;
        }
    }
}

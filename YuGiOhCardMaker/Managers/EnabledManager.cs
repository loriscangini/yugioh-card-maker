﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    class EnabledManager
    {
        private readonly Card card;

        public EnabledManager(Card card)
        {
            this.card = card;
        }

        private void Disable(CheckBox box)
        {
            box.IsChecked = false;
            box.IsEnabled = false;
        }

        public void SetPendulumCheckBoxes(CheckBox pendulum, CheckBox deckMaster, Control options)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                case Series.Rush:
                    Disable(pendulum);
                    Disable(deckMaster);
                    break;
                default:
                    if (card.CanPendulum && !card.IsPendulum)
                        deckMaster.IsEnabled = true;
                    else
                        Disable(deckMaster);

                    if (card.CanPendulum && !card.IsDeckMaster)
                        pendulum.IsEnabled = true;
                    else
                        Disable(pendulum);
                    break;
            }
            options.IsEnabled = card.IsPendulum || card.IsDeckMaster;
        }

        public void SetMaximumCheckBox(CheckBox maximum)
        {
            if (card.HasATK && card.Series != Series.ForKids)
                maximum.IsEnabled = true;
            else
                Disable(maximum);
        }

        public void SetFullCheckBox(CheckBox full)
        {
            switch (card.Series)
            {
                case Series.Rush:
                    Disable(full);
                    break;
                default:
                    if (card.IsSmallPendulumImage || card.IsCustom)
                        Disable(full);
                    else
                        full.IsEnabled = true;
                    break;
            }
        }

        public void SetSmallImageCheckBox(CheckBox smallImage)
        {
            if ((card.IsPendulum || card.IsDeckMaster) && !card.IsFullImage && card.Series != Series.ForKids)
                smallImage.IsEnabled = true;
            else
                Disable(smallImage);
        }

        public void SetGoldPlatinumCheckBoxes(CheckBox gold, CheckBox platinum)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                case Series.Rush:
                    Disable(gold);
                    Disable(platinum);
                    break;
                default:
                    if (!card.IsGoldRare)
                        platinum.IsEnabled = true;
                    else
                        Disable(platinum);

                    if (!card.IsPlatinumRare)
                        gold.IsEnabled = true;
                    else
                        Disable(gold);
                    break;
            }
        }

        public void SetSpeedSpellCheckBox(CheckBox speedSpell)
        {
            if (card.Series != Series.Rush && card.HasTopType && !card.IsFullImage)
                speedSpell.IsEnabled = true;
            else
                Disable(speedSpell);
        }

        public void SetCustomCheckBoxes(CheckBox atk, CheckBox def, CheckBox link)
        {
            if (card.IsMonster)
            {
                atk.IsEnabled = true;

                if (link.IsChecked == true)
                    Disable(def);
                else
                    def.IsEnabled = true;
            }
            else
            {
                Disable(atk);
                Disable(def);
            }

            if (def.IsChecked == true)
                Disable(link);
            else
                link.IsEnabled = true;

        }
    }
}

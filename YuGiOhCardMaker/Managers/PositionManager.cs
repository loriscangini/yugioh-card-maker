﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker.Managers
{
    class PositionManager
    {
        private readonly Card card;

        public PositionManager(Card card)
        {
            this.card = card;
        }

        public void SetName(FrameworkElement name, FrameworkElement nameOutline)
        {
            switch (card.Series)
            {
                case Series.Rush:
                    name.Margin = Margins.Name.Rush;
                    nameOutline.Margin = Margins.Name.RushOutline;
                    break;
                default:
                    name.Margin = Margins.Name.Normal;
                    nameOutline.Margin = Margins.Name.NormalOutline;
                    break;
            }
        }
        public void SetAttribute(FrameworkElement attribute)
        {
            switch (card.Series)
            {
                case Series.ForKids:
                    attribute.Margin = card.IsMonster ? Margins.Attribute.KidsMonster : Margins.Attribute.KidsSpellTrap;
                    attribute.Width = Margins.Attribute.KidsSize;
                    attribute.Height = Margins.Attribute.KidsSize;
                    break;
                case Series.Rush:
                    attribute.Margin = Margins.Attribute.Rush;
                    attribute.Width = Margins.Attribute.RushSize;
                    attribute.Height = Margins.Attribute.RushSize;
                    break;
                default:
                    attribute.Margin = Margins.Attribute.Normal;
                    attribute.Width = Margins.Attribute.NormalSize;
                    attribute.Height = Margins.Attribute.NormalSize;
                    break;
            }
        }
        public void SetStars(Grid stars)
        {
            if (card.Series == Series.ForKids)
            {
                stars.Margin = Margins.Stars.Kids;
                foreach (var star in stars.Children.OfType<Image>())
                {
                    star.Width = Sizes.Star.Kids;
                    star.Height = Sizes.Star.Kids;
                } 
            }
            else
            {
                stars.Margin = Margins.Stars.Normal;
                foreach (var star in stars.Children.OfType<Image>())
                {
                    star.Width = Sizes.Star.Normal;
                    star.Height = Sizes.Star.Normal;
                }
            }
        }
        public void SetEvolute(FrameworkElement evolute1)
        {
            if (card.CardType == CardTypes.Evolute)
                evolute1.Margin = Margins.EvoluteFirstNumber.Normal;

            if (card.CardType == CardTypes.EvoluteConjoint)
                evolute1.Margin = Margins.EvoluteFirstNumber.Conjoint;
        }
        public void SetLinkArrows(Grid linkArrows)
        {
            void scaleArrows(double scale) {
                linkArrows.Children.OfType<Image>().ToList().ForEach(a => {
                    a.LayoutTransform = new ScaleTransform(scale, scale);
                });

            }

            if (card.Series == Series.Rush)
            {
                linkArrows.Margin = Margins.LinkArrows.Rush;
                scaleArrows(0.4);
            }
            else if (card.Series == Series.ForKids)
            {
                linkArrows.Margin = Margins.LinkArrows.Kids;
                scaleArrows(1);
            }
            else if (card.IsPendulum || card.IsDeckMaster)
            {
                linkArrows.Margin = Margins.LinkArrows.Pendulum;
                scaleArrows(1);
            }
            else
            {
                linkArrows.Margin = Margins.LinkArrows.Normal;
                scaleArrows(1);
            }
        }
        public void SetArtwork(FrameworkElement artwork, FrameworkElement fakeArtwork)
        {
            Panel.SetZIndex(artwork, 0);
            fakeArtwork.Visibility = Visibility.Hidden;
            if (card.IsFullImage)
                artwork.Margin = Margins.Artwork.Full;
            else if (card.Series == Series.Rush)
                artwork.Margin = Margins.Artwork.Rush;
            else if (card.Series == Series.ForKids)
                artwork.Margin = Margins.Artwork.Kids;
            else if (card.IsOriginalManga)
                artwork.Margin = Margins.Artwork.Manga;
            else if (card.IsPendulum || card.IsDeckMaster)
            {
                Panel.SetZIndex(artwork, 1);
                fakeArtwork.Visibility = Visibility.Visible;
                if (card.IsSmallPendulumImage)
                    artwork.Margin = Margins.Artwork.SmallPendulum(card.ActiveBoxSize);
                else
                    artwork.Margin = Margins.Artwork.BigPendulum;
            }
            else
                artwork.Margin = Margins.Artwork.Normal;
        }
        public void SetFoil(FrameworkElement foil, FrameworkElement artwork)
        {
            if (!card.IsFullImage && card.Foil.In(Foil.Default, Foil.Secret, Foil.Ultimate))
                foil.Margin = artwork.Margin;
            else
                foil.Margin = new Thickness();
        }
        public void SetLegend(FrameworkElement legend)
        {
            switch (card.Series)
            {
                case Series.Rush: legend.Margin = Margins.Legend.Rush; break;
                case Series.ForKids when card.CardType == CardTypes.Link: legend.Margin = Margins.Legend.KidsLink; break;
                case Series.ForKids: legend.Margin = Margins.Legend.Kids; break;
                default: legend.Margin = Margins.Legend.Normal; break;
            }
        }

        public void SetPendulumOrDeckMaster(FrameworkElement pendulumEffect, FrameworkElement leftScale, FrameworkElement rightScale)
        {
            if (card.IsPendulum)
            {
                if (card.Series == Series.ForKids)
                {
                    leftScale.Margin = Margins.LeftScale.Kids;
                    rightScale.Margin = Margins.RightScale.Kids;
                }
                else
                {
                    switch (card.ActiveBoxSize)
                    {
                        case BoxSize.Small:
                            pendulumEffect.Margin = Margins.PendulumBox.Small;
                            pendulumEffect.Height = Sizes.PendulumDeckMasterBox.SmallHeight;
                            leftScale.Margin = Margins.LeftScale.SmallPendulum;
                            rightScale.Margin = Margins.RightScale.SmallPendulum;
                            break;
                        case BoxSize.Medium:
                            pendulumEffect.Margin = Margins.PendulumBox.Medium;
                            pendulumEffect.Height = Sizes.PendulumDeckMasterBox.MediumHeight;
                            leftScale.Margin = Margins.LeftScale.MediumPendulum;
                            rightScale.Margin = Margins.RightScale.MediumPendulum;
                            break;
                        case BoxSize.Large:
                            pendulumEffect.Margin = Margins.PendulumBox.Large;
                            pendulumEffect.Height = Sizes.PendulumDeckMasterBox.LargeHeight;
                            leftScale.Margin = Margins.LeftScale.LargePendulum;
                            rightScale.Margin = Margins.RightScale.LargePendulum;
                            break;
                    }
                }
            }
            if (card.IsDeckMaster)
            {
                switch (card.ActiveBoxSize)
                {
                    case BoxSize.Small:
                        pendulumEffect.Margin = Margins.DeckMasterBox.Small;
                        pendulumEffect.Height = Sizes.PendulumDeckMasterBox.SmallHeight;
                        break;
                    case BoxSize.Medium:
                    case BoxSize.Large:
                        pendulumEffect.Margin = Margins.DeckMasterBox.Medium;
                        pendulumEffect.Height = Sizes.PendulumDeckMasterBox.MediumHeight;
                        break;
                }
            }
        }
        public void SetCardNumber(System.Windows.Controls.RichTextBox cardNumber)
        {
            if (card.Series == Series.Rush)
            {
                cardNumber.Margin = Margins.CardNumber.Rush;
                cardNumber.Document.TextAlignment = TextAlignment.Right;
                if (cardNumber.Document.Blocks.Count > 0)
                    cardNumber.Document.Blocks.FirstBlock.TextAlignment = TextAlignment.Right;
                cardNumber.HorizontalAlignment = HorizontalAlignment.Right;
            }
            else if (card.IsPendulum || card.IsDeckMaster || card.IsFullImage)
            {
                cardNumber.Margin = Margins.CardNumber.Pendulum;
                cardNumber.Document.TextAlignment = TextAlignment.Left;
                if (cardNumber.Document.Blocks.Count > 0)
                    cardNumber.Document.Blocks.FirstBlock.TextAlignment = TextAlignment.Left;
                cardNumber.HorizontalAlignment = HorizontalAlignment.Left;
            }
            else if (card.HasLinkArrows)
            {
                cardNumber.Margin = Margins.CardNumber.Link;
                cardNumber.Document.TextAlignment = TextAlignment.Right;
                if (cardNumber.Document.Blocks.Count > 0)
                    cardNumber.Document.Blocks.FirstBlock.TextAlignment = TextAlignment.Right;
                cardNumber.HorizontalAlignment = HorizontalAlignment.Right;
            }
            else
            {
                cardNumber.Margin = Margins.CardNumber.Normal;
                cardNumber.Document.TextAlignment = TextAlignment.Right;
                if (cardNumber.Document.Blocks.Count > 0)
                    cardNumber.Document.Blocks.FirstBlock.TextAlignment = TextAlignment.Right;
                cardNumber.HorizontalAlignment = HorizontalAlignment.Right;
            }
        }
        public void SetTypeAbilityLine(System.Windows.Controls.RichTextBox typeAbility)
        {
            switch (card.Series)
            {
                case Series.Rush:
                    typeAbility.Margin = Margins.TypeAbility.Rush;
                    break;
                default:
                    if (card.HasTopType)
                    {
                        typeAbility.Margin = Margins.TypeAbility.Top;

                        foreach (Paragraph p in typeAbility.Document.Blocks.OfType<Paragraph>())
                            p.TextAlignment = TextAlignment.Right;

                        typeAbility.HorizontalContentAlignment = HorizontalAlignment.Right;
                    }
                    else if (card.IsPendulum)
                    {
                        switch (card.ActiveBoxSize)
                        {
                            case BoxSize.Small:
                            case BoxSize.Medium: typeAbility.Margin = Margins.TypeAbility.Normal; break;
                            case BoxSize.Large: typeAbility.Margin = Margins.TypeAbility.BigPendulum; break;
                        }
                        foreach (Paragraph p in typeAbility.Document.Blocks.OfType<Paragraph>())
                            p.TextAlignment = TextAlignment.Left;
                    }
                    else
                    {
                        typeAbility.Margin = Margins.TypeAbility.Normal;
                        foreach (Paragraph p in typeAbility.Document.Blocks.OfType<Paragraph>())
                            p.TextAlignment = TextAlignment.Left;
                    }

                    break;
            }
        }
        public void SetEffectBox(FrameworkElement effect)
        {
            switch (card.Series)
            {
                case Series.Manga when card.IsOriginalManga:
                    effect.Margin = !card.CardType.In(CardTypes.OriginalSpellTrap)
                        ? Margins.EffectBox.FullMangaMonster
                        : Margins.EffectBox.FullMangaSpell;
                    break;
                case Series.Rush:
                    effect.Margin = Margins.EffectBox.Rush;
                    break;
                default:
                    if (card.HasTypeAbilityText)
                    {
                        if (card.CardType == CardTypes.Phasm)
                            effect.Margin = Margins.EffectBox.Phasm;

                        if (card.IsPendulum)
                        {
                            if (card.CardType == CardTypes.Phasm)
                            {
                                switch (card.ActiveBoxSize)
                                {
                                    case BoxSize.Small:
                                    case BoxSize.Medium: effect.Margin = Margins.EffectBox.Phasm; break;
                                    case BoxSize.Large: effect.Margin = Margins.EffectBox.PhasmBigPendulum; break;
                                }
                            }
                            else
                            {
                                switch (card.ActiveBoxSize)
                                {
                                    case BoxSize.Small:
                                    case BoxSize.Medium: effect.Margin = Margins.EffectBox.Normal; break;
                                    case BoxSize.Large: effect.Margin = Margins.EffectBox.Small; break;
                                }
                            }
                        }
                        else
                        {
                            effect.Margin = card.CardType == CardTypes.Phasm ? Margins.EffectBox.Phasm : Margins.EffectBox.Normal;
                        }
                    }
                    else
                        effect.Margin = Margins.EffectBox.WithoutTypeAbility;
                    break;
            }
        }
        public void SetBottomEffect(FrameworkElement bottomEffect)
        {
            switch (card.Series)
            {
                case Series.Rush:
                    bottomEffect.Margin = Margins.BottomEffectBox.Rush;
                    break;
                default:
                    bottomEffect.Margin = Margins.BottomEffectBox.Normal;
                    break;
            }
        }
        public void SetATKDEFLink(FrameworkElement ATK, FrameworkElement ATKLabel, FrameworkElement DEF, FrameworkElement DEFLabel, FrameworkElement linkNumber, FrameworkElement linkLabel, FrameworkElement maximumATK, FrameworkElement maximumATKLabel)
        {
            if (card.Series == Series.Rush)
            {
                ATK.Margin = Margins.ATK.Rush;
                DEF.Margin = Margins.DEF.Rush;
                linkNumber.Margin = Margins.LinkNumber.Rush;
                maximumATK.Margin = Margins.MaximumATK.Rush;
            }
            else if (card.Series == Series.ForKids)
            {
                ATK.Margin = Margins.ATK.Kids;
                DEF.Margin = Margins.DEF.Kids;
                linkNumber.Margin = Margins.DEF.Kids;
                linkLabel.Margin = Margins.LinkLabel.Kids;
            }
            else if (card.IsOriginalManga)
            {
                ATK.Margin = Margins.ATK.Manga;
                DEF.Margin = Margins.DEF.Manga;
                ATKLabel.Margin = Margins.ATK.LabelManga;
                DEFLabel.Margin = Margins.DEF.LabelManga;
                maximumATK.Margin = Margins.MaximumATK.Manga;
                maximumATKLabel.Margin = Margins.MaximumATK.LabelManga;
            }
            else
            {
                DEF.Margin = Margins.DEF.Normal;
                linkNumber.Margin = Margins.LinkNumber.Normal;
                linkLabel.Margin = Margins.LinkLabel.Normal(linkNumber.ActualWidth - linkNumber.MinWidth);

                DEFLabel.Margin = Margins.DEF.LabelNormal(DEF.ActualWidth - DEF.MinWidth);

                if (card.HasLinkArrows)
                {
                    ATK.Margin = Margins.ATK.Normal(linkNumber.ActualWidth - linkNumber.MinWidth);
                    ATKLabel.Margin = Margins.ATK.LabelNormal(ATK.ActualWidth - ATK.MinWidth + linkNumber.ActualWidth - linkNumber.MinWidth);
                    maximumATK.Margin = Margins.MaximumATK.Normal(ATK.ActualWidth - ATK.MinWidth + linkNumber.ActualWidth - linkNumber.MinWidth);
                    maximumATKLabel.Margin = Margins.MaximumATK.LabelNormal(maximumATK.ActualWidth - maximumATK.MinWidth + ATK.ActualWidth - ATK.MinWidth + linkNumber.ActualWidth - linkNumber.MinWidth);
                }
                else
                {
                    ATK.Margin = Margins.ATK.Normal(DEF.ActualWidth - DEF.MinWidth);
                    ATKLabel.Margin = Margins.ATK.LabelNormal(ATK.ActualWidth - ATK.MinWidth + DEF.ActualWidth - DEF.MinWidth);
                    maximumATK.Margin = Margins.MaximumATK.Normal(ATK.ActualWidth - ATK.MinWidth + DEF.ActualWidth - DEF.MinWidth);
                    maximumATKLabel.Margin = Margins.MaximumATK.LabelNormal(maximumATK.ActualWidth - maximumATK.MinWidth + ATK.ActualWidth - ATK.MinWidth + DEF.ActualWidth - DEF.MinWidth);
                }
            }
        }
        public void SetPasscodeEdition(System.Windows.Controls.RichTextBox passcode, FrameworkElement edition)
        {
            if (card.Series == Series.Rush)
            {
                passcode.Margin = Margins.Passcode.Rush;
                edition.Margin = Margins.Edition.Rush;
            }
            else
            {
                if (card.HasLinkArrows && (card.IsPendulum || card.IsDeckMaster))
                {
                    passcode.Margin = Margins.Passcode.LinkPendulum;
                    edition.Margin = Margins.Edition.LinkPendulum;
                }
                else if (card.HasLinkArrows)
                {
                    passcode.Margin = Margins.Passcode.Normal;
                    edition.Margin = passcode.Text().Length > 9 ? Margins.Edition.LongPasscodeLink : Margins.Edition.Normal;
                }
                else
                {
                    passcode.Margin = Margins.Passcode.Normal;
                    edition.Margin = passcode.Text().Length > 9 && !card.IsPendulum && !card.IsDeckMaster ? Margins.Edition.LongPasscode : Margins.Edition.Normal;
                }
            }
        }

        public void SetAttributeChooser(FrameworkElement chooser)
        {
            switch (card.Series)
            {
                case Series.ForKids:
                    chooser.Margin = Margins.AttributeChooser.Kids;
                    break;
                default:
                    chooser.Margin = Margins.AttributeChooser.Normal;
                    break;
            }
        }
    }


    public static class Margins
    {
        public static class Name
        {
            public static Thickness Normal = new Thickness(38, 31, 0, 0);
            public static Thickness Rush = new Thickness(29, 16, 0, 0);
            public static Thickness NormalOutline = new Thickness(43, 61, 0, 0);
            public static Thickness RushOutline = new Thickness(34, 46, 0, 0);
        }
        public static class Stars
        {
            public static Thickness Normal = new Thickness(0, 93, 0, 0);
            public static Thickness Kids = new Thickness(0, 625, 90, 0);
        }
        public static class EvoluteFirstNumber
        {
            public static Thickness Normal = new Thickness(237, 100, 243, 0);
            public static Thickness Conjoint = new Thickness(208, 100, 272, 0);
        }
        public static class Attribute
        {
            public static Thickness KidsMonster = new Thickness(440, 615, 0, 0);
            public static Thickness KidsSpellTrap = new Thickness(242.5, 670, 0, 0);
            public static int KidsSize = 65;
            public static Thickness Normal = new Thickness(460, 36, 0, 0);
            public static int NormalSize = 52;
            public static Thickness Rush = new Thickness(452, 29, 0, 0);
            public static int RushSize = 67;
        }
        public static class Artwork
        {
            public static Thickness Normal = new Thickness(65, 144, 65, 236);
            public static Thickness Manga = new Thickness(65, 144, 65, 294);
            public static Thickness Rush = new Thickness(29, 80, 29, 225);
            public static Thickness Kids = new Thickness(16, 16, 16, 214);
            public static Thickness BigPendulum = new Thickness(38, 142, 37, 186);
            public static Thickness Full = new Thickness();

            public static Thickness SmallPendulum(BoxSize activeBoxSize)
            {
                switch (activeBoxSize)
                {
                    case BoxSize.Small: return new Thickness(38, 144, 37, 270);
                    case BoxSize.Medium:
                    case BoxSize.Large: return new Thickness(38, 144, 37, 299);
                    default: return new Thickness();
                }
            }
        }
        public static class Legend
        {
            public static Thickness Normal = new Thickness(70, 150, 0, 0);
            public static Thickness Rush = new Thickness(32, 85, 0, 0);
            public static Thickness Kids = new Thickness(20, 20, 0, 0);
            public static Thickness KidsLink = new Thickness(50, 20, 0, 0);
        }
        public static class LinkArrows
        {
            public static Thickness Normal = new Thickness(35, 115, 34, 205);
            public static Thickness Pendulum = new Thickness(7, 113, 6, 16);
            public static Thickness Rush = new Thickness(25, 490, 430, 220);
            public static Thickness Kids = new Thickness(0, 0, 0, 200);
        }
        public static class PendulumBox
        {
            public static Thickness Small = new Thickness(79, 530, 79, 0);
            public static Thickness Medium = new Thickness(79, 504, 79, 0);
            public static Thickness Large = Medium;
        }
        public static class DeckMasterBox
        {
            public static Thickness Small = new Thickness(39, 530, 39, 0);
            public static Thickness Medium = new Thickness(39, 504, 39, 0);
            public static Thickness Large = Medium;
        }
        public static class LeftScale
        {
            public static Thickness SmallPendulum = new Thickness(40, 562, 472, 0);
            public static Thickness MediumPendulum = new Thickness(40, 545, 472, 0);
            public static Thickness LargePendulum = new Thickness(40, 556, 472, 0);
            public static Thickness Kids = new Thickness(15, 555, 490, 0);
        }
        public static class RightScale
        {
            public static Thickness SmallPendulum = new Thickness(472, 562, 40, 0);
            public static Thickness MediumPendulum = new Thickness(472, 545, 40, 0);
            public static Thickness LargePendulum = new Thickness(472, 556, 40, 0);
            public static Thickness Kids = new Thickness(490, 555, 15, 0);
        }
        public static class TypeAbility
        {
            public static Thickness Top = new Thickness(56, 97, 56, 0);
            public static Thickness Normal = new Thickness(40, 599, 40, 0);
            public static Thickness BigPendulum = new Thickness(40, 620, 40, 0);
            public static Thickness Rush = new Thickness(34, 576, 110, 0);
        }
        public static class EffectBox
        {
            public static Thickness Normal = new Thickness(39, 622, 39, 73);
            public static Thickness Small = new Thickness(39, 643, 39, 73);
            public static Thickness Phasm = new Thickness(39, 622, 39, 48);
            public static Thickness PhasmBigPendulum = new Thickness(39, 643, 39, 48);
            public static Thickness WithoutTypeAbility = new Thickness(39, 599, 39, 48);
            public static Thickness FullMangaMonster = new Thickness(39, 517, 39, 73);
            public static Thickness FullMangaSpell = new Thickness(39, 517, 39, 34);
            public static Thickness Rush = new Thickness(32, 609, 32, 55);
        }
        public static class UpgradeStage
        {
            public static Thickness Normal = new Thickness(44, 599, 44, 0);
            public static Thickness BigPendulum = new Thickness(44, 620, 44, 0);
        }

        public static class BottomEffectBox
        {
            public static Thickness Normal = new Thickness(39, 0, 39, 72);
            public static Thickness Rush = new Thickness(32, 0, 32, 55);
        }
        public static class ATK
        {
            public static Thickness Normal(double delta) => new Thickness(0, 0, 155 + delta, 48);
            public static Thickness LabelNormal(double delta) => new Thickness(0, 0, 207 + delta, 48);

            public static Thickness Rush = new Thickness(0, 0, 260, 222);

            public static Thickness Kids = new Thickness(0, 0, 315, 38);

            public static Thickness Manga = new Thickness(0, 0, 290, 35);
            public static Thickness LabelManga = new Thickness(0, 0, 390, 35);
        }
        public static class DEF
        {
            public static Thickness Normal = new Thickness(0, 0, 42, 48);
            public static Thickness LabelNormal(double delta) => new Thickness(0, 0, 95 + delta, 48);

            public static Thickness Rush = new Thickness(0, 0, 90, 222);

            public static Thickness Kids = new Thickness(0, 0, 65, 38);

            public static Thickness Manga = new Thickness(0, 0, 97, 35);
            public static Thickness LabelManga = new Thickness(0, 0, 200, 35);
        }
        public static class LinkLabel
        {
            public static Thickness Normal(double delta) => new Thickness(0, 0, 63 + delta, 48);
            public static Thickness Kids = new Thickness(0, 0, 100, 38);
        }
        public static class LinkNumber
        {
            public static Thickness Normal = new Thickness(0, 0, 40, 40);

            public static Thickness Rush = DEF.Rush;
        }
        public static class MaximumATK
        {
            public static Thickness Normal(double delta) => new Thickness(0, 0, 265 + delta, 48);
            public static Thickness LabelNormal(double delta) => new Thickness(0, 0, 317 + delta, 48);

            public static Thickness Rush = new Thickness(0, 0, 100, 260);

            public static Thickness Manga = new Thickness(0, 0, 97, 80);
            public static Thickness LabelManga = new Thickness(0, 0, 195, 80);
        }
        public static class CardNumber
        {
            public static Thickness Normal = new Thickness(0, 574, 57, 0);
            public static Thickness Link = new Thickness(0, 574, 96, 0);
            public static Thickness Pendulum = new Thickness(42, 731, 0, 0);
            public static Thickness Rush = new Thickness(0, 752, 57, 0);
        }
        public static class Passcode
        {
            public static Thickness Normal = new Thickness(23, 761, 0, 0);
            public static Thickness LinkPendulum = new Thickness(70, 761, 0, 0);
            public static Thickness Rush = new Thickness(32, 751, 0, 0);
        }
        public static class Edition
        {
            public static Thickness Normal = new Thickness(113, 761, 0, 0);
            public static Thickness LinkPendulum = new Thickness(160, 761, 0, 0);
            public static Thickness LongPasscode = new Thickness(57, 572, 0, 0);
            public static Thickness LongPasscodeLink = new Thickness(96, 572, 0, 0);
            public static Thickness Rush = new Thickness(113, 751, 0, 0);
        }


        public static class AttributeChooser
        {
            public static Thickness Normal = new Thickness(0, 80, 200, 0);
            public static Thickness Kids = new Thickness(0, 440, 200, 0);
        }
    }

    public static class Sizes
    {
        public static class Star
        {
            public static int Normal = 34;
            public static int Kids = 28;
        }
        public static class PendulumDeckMasterBox
        {
            public static int SmallHeight = 65;
            public static int MediumHeight = 91;
            public static int LargeHeight = 111;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using YuGiOhCardMaker.Managers;

namespace YuGiOhCardMaker
{
    /// <summary>
    /// Logica di interazione per TextEditor.xaml
    /// </summary>
    public partial class TextEditor : Window
    {
        public IEnumerable<FontFamily> AllFonts => Managers.Fonts.CustomFonts.Union(System.Windows.Media.Fonts.SystemFontFamilies);

        public ObservableCollection<ColorItem> UsefulColors => new ObservableCollection<ColorItem>()
        {
            new ColorItem(Colors.Transparent, "Transparent"),
            new ColorItem(Colors.Black, "Black"),
            new ColorItem(Colors.White, "White"),
            new ColorItem(Colors.Silver, "Silver"),
            new ColorItem(Colors.Gold, "Gold"),
            new ColorItem((Color)ColorConverter.ConvertFromString("#e8264b"), "Phasm material"),
            new ColorItem(Colors.Gray, "Dark Synchro")
        };

        public Func<FontFamily, string> FontSimpleName => f => f.Source.Split('/').Last();

        public TextEditor(VisibilityManager visibilityManager, ColorManager colorManager)
        {
            this.visibilityManager = visibilityManager;
            this.colorManager = colorManager;

            InitializeComponent();
        }

        public void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void DisableEvents()
        {
            fontFamily.SelectionChanged -= UpdateFont;
            fontSize.ValueChanged -= UpdateFont;
            fontColor.SelectedColorChanged -= UpdateFont;
            gradientColor1.SelectedColorChanged -= UpdateOutline;
            gradientColor2.SelectedColorChanged -= UpdateOutline;
            gradientColor3.SelectedColorChanged -= UpdateOutline;
            gradientOutlineColor.SelectedColorChanged -= UpdateOutline;
            gradientOutlineWidth.ValueChanged -= UpdateOutline;
            gradientOffset1.ValueChanged -= UpdateOutline;
            gradientOffset2.ValueChanged -= UpdateOutline;
            gradientOffset3.ValueChanged -= UpdateOutline;
            SpecialColor.SelectionChanged -= UpdateOutline;
            tabColors.SelectionChanged -= TabColor_SelectionChanged;
        }
        private void EnableEvents()
        {
            fontFamily.SelectionChanged += UpdateFont;
            fontSize.ValueChanged += UpdateFont;
            fontColor.SelectedColorChanged += UpdateFont;
            gradientColor1.SelectedColorChanged += UpdateOutline;
            gradientColor2.SelectedColorChanged += UpdateOutline;
            gradientColor3.SelectedColorChanged += UpdateOutline;
            gradientOutlineColor.SelectedColorChanged += UpdateOutline;
            gradientOutlineWidth.ValueChanged += UpdateOutline;
            gradientOffset1.ValueChanged += UpdateOutline;
            gradientOffset2.ValueChanged += UpdateOutline;
            gradientOffset3.ValueChanged += UpdateOutline;
            SpecialColor.SelectionChanged += UpdateOutline;
            tabColors.SelectionChanged += TabColor_SelectionChanged;
        }


        private Control lastBox;
        private OutlineTextControl outline;
        private readonly VisibilityManager visibilityManager;
        private readonly ColorManager colorManager;

        public void Update(Control box, OutlineTextControl outline)
        {
            bool IsSameFont(FontFamily b1, FontFamily b2) => FontSimpleName(b1) == FontSimpleName(b2);

            DisableEvents();

            lastBox = box;
            this.outline = outline;

            if (box is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = box as System.Windows.Controls.RichTextBox;
                fontFamily.SelectedItem = rtb.Selection.GetPropertyValue(FontFamilyProperty) is FontFamily ff ? AllFonts.First(font => IsSameFont(font, ff)) : null;
                fontSize.Value = rtb.Selection.GetPropertyValue(FontSizeProperty) as double?;
                fontColor.SelectedColor = (rtb.Selection.GetPropertyValue(ForegroundProperty) as SolidColorBrush)?.Color;
            }
            else if(box != null)
            {
                fontFamily.SelectedItem = AllFonts.First(font => IsSameFont(font, box.FontFamily));
                fontSize.Value = box.FontSize;
                fontColor.SelectedColor = (box.Foreground as SolidColorBrush)?.Color;
            }

            if (outline != null)
            {
                switch (outline.Fill)
                {
                    case SolidColorBrush scb:
                        gradientColor1.SelectedColor = scb.Color;
                        gradientColor2.SelectedColor = scb.Color;
                        gradientColor3.SelectedColor = scb.Color;
                        break;
                    case LinearGradientBrush lgb:
                        gradientAngle.Value = lgb.Angle();
                        gradientColor1.SelectedColor = lgb.GradientStops[0].Color;
                        gradientOffset1.Value = lgb.GradientStops[0].Offset;
                        gradientColor2.SelectedColor = lgb.GradientStops[1].Color;
                        gradientOffset2.Value = lgb.GradientStops[1].Offset;
                        gradientColor3.SelectedColor = lgb.GradientStops[2].Color;
                        gradientOffset3.Value = lgb.GradientStops[2].Offset;
                        break;
                }
                gradientOutlineColor.SelectedColor = (outline.Stroke as SolidColorBrush)?.Color;
                gradientOutlineWidth.Value = outline.StrokeThickness;
            }

            tabSolid.IsEnabled = box != null;
            tabGradient.IsEnabled = outline != null;
            tabSpecial.IsEnabled = outline != null;
            if (outline != null && fontColor.SelectedColor == Colors.Transparent)
            {
                if (outline.Fill is ImageBrush)
                    tabColors.SelectedItem = tabSpecial;
                else
                    tabColors.SelectedItem = tabGradient;
            }
            else
                tabColors.SelectedItem = tabSolid;

            EnableEvents();
        }

        private void UpdateFont(object sender, EventArgs e)
        {
            if (lastBox is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = lastBox as System.Windows.Controls.RichTextBox;
                if (fontFamily.SelectedItem != null)
                    rtb.Selection.ApplyPropertyValue(FontFamilyProperty, fontFamily.SelectedItem);
                if (fontSize.Value.HasValue)
                    rtb.Selection.ApplyPropertyValue(FontSizeProperty, fontSize.Value.Value);
                if (fontColor.SelectedColor.HasValue)
                    rtb.Selection.ApplyPropertyValue(ForegroundProperty, new SolidColorBrush(fontColor.SelectedColor.Value));
            }
            else if (lastBox != null)
            {
                if (fontFamily.SelectedItem != null)
                    lastBox.FontFamily = fontFamily.SelectedItem as FontFamily;
                if (fontSize.Value.HasValue)
                    lastBox.FontSize = fontSize.Value.Value;
                if (fontColor.SelectedColor.HasValue)
                    lastBox.Foreground = new SolidColorBrush(fontColor.SelectedColor.Value);
            }

            if (outline != null)
            {
                if (fontFamily.SelectedItem != null)
                    outline.Font = fontFamily.SelectedItem as FontFamily;
                if (fontSize.Value.HasValue)
                    outline.FontSize = fontSize.Value.Value;
            }
        }

        private void UseNameFont(object sender, RoutedEventArgs e)
        {
            fontFamily.SelectedItem = Managers.Fonts.MatrixRegularSmallCaps;
            fontSize.Value = 60;
        }
        private void UseSymbolFont(object sender, RoutedEventArgs e)
        {
            fontFamily.SelectedItem = Managers.Fonts.DFLeisho4;
            fontSize.Value = 40;
        }
        private void UseAtFont(object sender, RoutedEventArgs e)
        {
            fontFamily.SelectedItem = Managers.Fonts.SanDiegoStAztecs;
            fontSize.Value = 45;
        }
        private void UseLoreFont(object sender, RoutedEventArgs e)
        {
            fontFamily.SelectedItem = Managers.Fonts.StoneSerif;
            fontSize.Value = 16;
            if (lastBox is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = lastBox as System.Windows.Controls.RichTextBox;
                rtb.Selection.ApplyPropertyValue(FontStyleProperty, FontStyles.Italic);
            }
            else
            {
                lastBox.FontStyle = FontStyles.Italic;
            }
        }
        private void UseEffectFont(object sender, RoutedEventArgs e)
        {
            fontFamily.SelectedItem = Managers.Fonts.MatrixBook;
            fontSize.Value = 16;
            if (lastBox is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = lastBox as System.Windows.Controls.RichTextBox;
                rtb.Selection.ApplyPropertyValue(FontStyleProperty, FontStyles.Normal);
            }
            else
            {
                lastBox.FontStyle = FontStyles.Normal;
            }
        }

        private void ToggleBold(object sender, RoutedEventArgs e)
        {
            if (lastBox is System.Windows.Controls.RichTextBox)
                EditingCommands.ToggleBold.Execute(null, lastBox);
            else
                lastBox.FontWeight = lastBox.FontWeight == FontWeights.Bold ? FontWeights.Normal : FontWeights.Bold;
        }

        private void ToggleItalic(object sender, RoutedEventArgs e)
        {
            if (lastBox is System.Windows.Controls.RichTextBox)
                EditingCommands.ToggleItalic.Execute(null, lastBox);
            else
                lastBox.FontStyle = lastBox.FontStyle == FontStyles.Italic ? FontStyles.Normal : FontStyles.Italic;
        }

        private void ToggleUnderline(object sender, RoutedEventArgs e) => EditingCommands.ToggleUnderline.Execute(null, lastBox);
        private void ToggleLeftAlign(object sender, RoutedEventArgs e) => EditingCommands.AlignLeft.Execute(null, lastBox);
        private void ToggleRightAlign(object sender, RoutedEventArgs e) => EditingCommands.AlignRight.Execute(null, lastBox);
        private void ToggleCenterAlign(object sender, RoutedEventArgs e) => EditingCommands.AlignCenter.Execute(null, lastBox);
        private void ToggleJustify(object sender, RoutedEventArgs e) => EditingCommands.AlignJustify.Execute(null, lastBox);
        private void ToggleSuperscript(object sender, RoutedEventArgs e)
        {
            if (lastBox is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = lastBox as System.Windows.Controls.RichTextBox;
                if ((BaselineAlignment)rtb.Selection.GetPropertyValue(Inline.BaselineAlignmentProperty) != BaselineAlignment.Superscript)
                    rtb.Selection.ApplyPropertyValue(Inline.BaselineAlignmentProperty, BaselineAlignment.Superscript);
                else
                    rtb.Selection.ApplyPropertyValue(Inline.BaselineAlignmentProperty, BaselineAlignment.Baseline);
            }
        }
        private void ToggleSubscript(object sender, RoutedEventArgs e)
        {
            if (lastBox is System.Windows.Controls.RichTextBox)
            {
                System.Windows.Controls.RichTextBox rtb = lastBox as System.Windows.Controls.RichTextBox;
                if ((BaselineAlignment)rtb.Selection.GetPropertyValue(Inline.BaselineAlignmentProperty) != BaselineAlignment.Subscript)
                    rtb.Selection.ApplyPropertyValue(Inline.BaselineAlignmentProperty, BaselineAlignment.Subscript);
                else
                    rtb.Selection.ApplyPropertyValue(Inline.BaselineAlignmentProperty, BaselineAlignment.Baseline);
            }
        }

        private LinearGradientBrush GetGradient()
        {
            GradientStopCollection gsc = new GradientStopCollection(new List<GradientStop>() {
                new GradientStop(gradientColor1?.SelectedColor ?? Colors.Black, gradientOffset1?.Value ?? 0),
                new GradientStop(gradientColor2?.SelectedColor ?? Colors.Black, gradientOffset2?.Value ?? 0),
                new GradientStop(gradientColor3?.SelectedColor ?? Colors.Black, gradientOffset3?.Value ?? 0)
            });

            return new LinearGradientBrush(gsc, gradientAngle.Value ?? 15);
        }
        private ImageBrush GetImage() => new ImageBrush((SpecialColor?.SelectedItem as Image)?.Source);

        private void UpdateOutline() 
        {
            Brush color = tabColors.SelectedItem == tabGradient ? GetGradient() as Brush : GetImage() as Brush;

            Color outlineStroke = gradientOutlineColor?.SelectedColor ?? Colors.Black;
            double outlineWidth = gradientOutlineWidth?.Value ?? 0;
            colorManager.SetOutline(outline, color, outlineStroke, outlineWidth);
        }
        private void UpdateOutline(object sender, RoutedPropertyChangedEventArgs<Color?> e) => UpdateOutline();
        private void UpdateOutline(object sender, RoutedPropertyChangedEventArgs<object> e) => UpdateOutline();
        private void UpdateOutline(object sender, SelectionChangedEventArgs e) => UpdateOutline();

        private void TabColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabColors.SelectedItem == tabSolid)
            {
                visibilityManager.SetOutline(outline, false);
            }
            else
            {
                fontColor.SelectedColor = Colors.Transparent;
                visibilityManager.SetOutline(outline, true);
                UpdateOutline();
            }
        }
    }

    public class FontSimpleName : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value as FontFamily)?.Source.Split('/').Last().Replace("#","");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}

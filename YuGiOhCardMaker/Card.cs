﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace YuGiOhCardMaker
{
    public class Card
    {
        public static class Enums
        {
            public static class Series
            {
                public const string Manga = "Manga";
                public const string ForKids = "4Kids";
                public const string Series9 = "Series 9";
                public const string Series10 = "Series 10";
                public const string Rush = "Rush Duel";

                public static string[] all = new string[] { Manga, ForKids, Series9, Series10, Rush }; 
            }
            public enum CardTypes
            {
                //Regular monsters
                Normal,
                Effect,
                Ritual,
                Fusion,
                Synchro,
                DarkSynchro,
                Xyz,
                Link,
                Token,
                Slifer,
                Obelisk,
                Ra,
                RaChant,
                WickedGod,
                LegendaryDragon,
                ZArcFusionSynchroXyz,
                ZArcFusionSynchro,
                ZArcFusion,
                OriginalMonster,
                OriginalXyz,
                OriginalDarkSynchro,

                //Custom monsters
                CustomMonster,

                Evolute,
                EvoluteConjoint,
                Criplify,
                Evolution,
                Phasm,

                //Regular Spell/Traps
                Spell,
                LinkSpell,
                Trap,
                OriginalSpellTrap,

                //Custom Spell/Traps
                CustomSpellTrap,

                TrapSpell,
                Virus,
                LinkTrap,
                LinkTrapSpell,
                King,

                //Regular Others
                Skill,

                //Custom Others
            }
            public enum Editions
            {
                None, Unlimited, First, Limited, Terminal
            }
            public enum StarTypes
            {
                Level, Rank, NegativeLevel, /*Grade, Class, Spectrum, Quality, Neon,*/ Evolute, EvoStar, /*AbsoluteLevel,*/ Vigor, None
            }
            public enum Attributes { Dark, Divine, Earth, Fire, Light, Water, Wind, Laugh, Spell, Trap, TrapSpell, Virus, King, Custom }
            public enum Symbols { Continuous, Counter, Equip, Field, Normal, QuickPlay, Ritual, Splice, Shield, Arcade }
            public enum Types
            {
                Aqua, Beast, BeastWarrior, CreatorGod, Cyberse, Dinosaur, DivineBeast, Dragon,
                Fairy, Fiend, Fish, Insect, Machine, Plant, Psychic, Pyro, Reptile, Rock, SeaSerpent, Spellcaster,
                Thunder, Warrior, WingedBeast, Wyrm, Zombie, Charisma, BlackMagic, DragonMagic, Human, IllusionMagic,
                Immortal, SeaBeast, WhiteMagic, Yokai, Qmarks, Spell, Trap
            }
            public enum Abilities { Toon, Spirit, Union, Gemini, Flip, Normal, Effect, Pendulum, DeckMaster, Maximum, Tuner, DarkTuner, Armor, Plus, Minus, Conjoint, Base }
            public enum BoxSize
            {
                Small, Medium, Large
            }
            public enum Foil { None, Default, Egypt, Mosaic, Parallel, Secret, Star, Ultimate }
        }

        public Enums.CardTypes CardType { get; set; } = Enums.CardTypes.Normal;
        public Enums.Attributes Attribute { get; set; } = Enums.Attributes.Dark;
        // public Enums.Symbols Symbol1 { get; set; } = Enums.Symbols.Normal;
        // public Enums.Symbols Symbol2 { get; set; } = Enums.Symbols.Normal;
        public int Stars { get; set; } = 4;

        public bool[] LinkArrows { get; set; } = new bool[8] { false, false, false, false, false, false, false, false };

        public int LinkRating => LinkArrows.Count(b => b == true);
        public string Type { get; set; } = "Type";
        public HashSet<string> Abilities { get; set; } = new HashSet<string>();
        public IEnumerable<string> SortedAbilities
        {
            get
            {
                HashSet<string> fix = new HashSet<string>(Abilities);
                if (Abilities.Contains(Enums.Abilities.Normal.ToNiceString()) && 
                        Abilities.Except(new List<string>() { Enums.Abilities.Normal.ToNiceString() }).Any(a => BindingValues.AbilityNames.Contains(a))
                    )
                    fix.Remove(Enums.Abilities.Normal.ToNiceString());
                return fix.Where(s => !string.IsNullOrWhiteSpace(s)).OrderBy(ab => ab.Rank());
            }
        }

        public Enums.Editions Edition { get; set; } = Enums.Editions.Unlimited;
        public Enums.BoxSize ActiveBoxSize { get; set; } = Enums.BoxSize.Small;
        public string Series { get; set; } = BindingValues.DefaultSeries;
        public Enums.Foil Foil { get; set; } = Enums.Foil.None;
        public bool IsGoldRare { get; set; } = false;
        public bool IsPlatinumRare { get; set; } = false;
        public bool IsPendulum { get; set; } = false;
        public bool IsMaximum { get; set; } = false;

        public void UpdateAbilities(Enums.CardTypes oldType)
        {
            Abilities.ExceptWith(oldType.GetAbilities());
            Abilities.UnionWith(CardType.GetAbilities());
        }

        public bool IsDeckMaster { get; set; } = false;
        public bool IsFullImage { get; set; } = false;
        public bool IsSmallPendulumImage { get; set; } = false;
        public bool IsSpeedDuel { get; set; } = false;
        public bool IsSpeedSpell { get; set; } = false;
        public bool IsLegend { get; set; } = false;

        public bool IsMonster => SeriesCompatibilities.Monsters.Contains(CardType);
        public bool IsCustom => CardType.In(Enums.CardTypes.CustomMonster, Enums.CardTypes.CustomSpellTrap);
        public bool CanPendulum => IsMonster && !IsOriginalManga && !CardType.In(Enums.CardTypes.Token, Enums.CardTypes.Criplify, Enums.CardTypes.RaChant);
        public bool HasTopType => CardType.In(Enums.CardTypes.Spell, Enums.CardTypes.Trap, Enums.CardTypes.Virus, Enums.CardTypes.TrapSpell, Enums.CardTypes.OriginalSpellTrap, Enums.CardTypes.King);
        public bool HasSpellTrapSymbol => HasTopType && !CardType.In(Enums.CardTypes.OriginalSpellTrap);
        public bool HasTypeAbilityText => (IsMonster && !IsOriginalManga) || CardType.In(Enums.CardTypes.Skill);
        public bool HasBottomEffect => CardType.In(Enums.CardTypes.Token, Enums.CardTypes.Normal);
        public bool HasAttribute => !CardType.In(Enums.CardTypes.Skill);
        public bool CustomHasATK { get; set; } = true;
        public bool CustomHasDEF { get; set; } = true;
        public bool CustomHasLink { get; set; } = false;
        public bool HasATK => (!IsCustom && IsMonster && !CardType.In(Enums.CardTypes.Phasm, Enums.CardTypes.RaChant)) || (IsCustom && CustomHasATK);
        public bool HasDEF => (!IsCustom && IsMonster && !CardType.In(Enums.CardTypes.Phasm, Enums.CardTypes.Link, Enums.CardTypes.RaChant)) || (IsCustom && CustomHasDEF);
        public bool HasLinkArrows => (!IsCustom && CardType.In(Enums.CardTypes.Link, Enums.CardTypes.LinkSpell, Enums.CardTypes.LinkTrap, Enums.CardTypes.LinkTrapSpell)) || (IsCustom && CustomHasLink);
        public bool HasDarkBackColor => CardType.In(Enums.CardTypes.Xyz, Enums.CardTypes.DarkSynchro, Enums.CardTypes.WickedGod, Enums.CardTypes.Skill, Enums.CardTypes.Virus, Enums.CardTypes.OriginalXyz);
        public bool HasWhiteName => HasDarkBackColor || CardType.In(Enums.CardTypes.Spell, Enums.CardTypes.Trap, Enums.CardTypes.LinkSpell, Enums.CardTypes.TrapSpell, Enums.CardTypes.LinkTrap,
            Enums.CardTypes.LinkTrapSpell);
        public bool IsOriginalManga => CardType.In(Enums.CardTypes.OriginalMonster, Enums.CardTypes.OriginalDarkSynchro, Enums.CardTypes.OriginalXyz, Enums.CardTypes.OriginalSpellTrap);

        public HorizontalAlignment CustomStarPosition { get; set; } = HorizontalAlignment.Right;
        public bool HasRightAlignedStars => (!IsCustom && StarType.In(Enums.StarTypes.Level)) || (IsCustom && CustomStarPosition == HorizontalAlignment.Right);
        public bool HasCenteredStars => (IsCustom && CustomStarPosition == HorizontalAlignment.Center) || Series == Enums.Series.ForKids;

        public Enums.StarTypes StarType
        {
            get
            {
                if (!IsMonster || HasLinkArrows)
                    return Enums.StarTypes.None;
                else switch (CardType)
                    {
                        case Enums.CardTypes.Xyz:
                        case Enums.CardTypes.OriginalXyz:
                            return Enums.StarTypes.Rank;
                        case Enums.CardTypes.DarkSynchro:
                        case Enums.CardTypes.OriginalDarkSynchro:
                            return Enums.StarTypes.NegativeLevel;
                        case Enums.CardTypes.Evolute:
                        case Enums.CardTypes.EvoluteConjoint:
                            return Enums.StarTypes.Evolute;
                        case Enums.CardTypes.Phasm: return Enums.StarTypes.Vigor;
                        case Enums.CardTypes.Link: return Enums.StarTypes.None;
                        default: return Enums.StarTypes.Level;
                    }
            }
        }

        public IEnumerable<IEnumerable<Enums.CardTypes>> GetCompatibleCardTypes()
        {
            List<Enums.CardTypes> empty = new List<Enums.CardTypes>();

            SeriesCompatibilities.RegularMonsters.TryGetValue(Series, out List<Enums.CardTypes> rm);
            SeriesCompatibilities.CustomMonsters.TryGetValue(Series, out List<Enums.CardTypes> cm);
            SeriesCompatibilities.RegularSpellTraps.TryGetValue(Series, out List<Enums.CardTypes> rst);
            SeriesCompatibilities.CustomSpellTraps.TryGetValue(Series, out List<Enums.CardTypes> cst);
            SeriesCompatibilities.RegularOthers.TryGetValue(Series, out List<Enums.CardTypes> ro);
            SeriesCompatibilities.CustomOthers.TryGetValue(Series, out List<Enums.CardTypes> co);

            return new List<IEnumerable<Enums.CardTypes>>() { rm ?? empty, cm ?? empty, rst ?? empty, cst ?? empty, ro ?? empty, co ?? empty };
        }
    }

    static class CardStatic
    {
        public static string Prettify(this Card.Enums.CardTypes type)
        {
            switch (type)
            {
                case Card.Enums.CardTypes.RaChant: return "Ra (Chant)";
                case Card.Enums.CardTypes.DarkSynchro: return "Dark Synchro";
                case Card.Enums.CardTypes.LegendaryDragon: return "Legendary Dragon";
                case Card.Enums.CardTypes.ZArcFusionSynchroXyz: return "Z-Arc (Fusion + Synchro + Xyz)";
                case Card.Enums.CardTypes.ZArcFusionSynchro: return "Z-Arc (Fusion + Synchro)";
                case Card.Enums.CardTypes.ZArcFusion: return "Z-Arc (Fusion)";
                case Card.Enums.CardTypes.OriginalMonster: return "Original Monster";
                case Card.Enums.CardTypes.OriginalXyz: return "Original Xyz";
                case Card.Enums.CardTypes.OriginalDarkSynchro: return "Original Dark Synchro";
                case Card.Enums.CardTypes.WickedGod: return "Wicked God";
                case Card.Enums.CardTypes.EvoluteConjoint: return "Evolute Conjoint";
                case Card.Enums.CardTypes.LinkSpell: return "Link Spell";
                case Card.Enums.CardTypes.TrapSpell: return "Trap Spell";
                case Card.Enums.CardTypes.LinkTrap: return "Link Trap";
                case Card.Enums.CardTypes.LinkTrapSpell: return "Link Trap Spell";
                case Card.Enums.CardTypes.OriginalSpellTrap: return "Original Spell/Trap";
                case Card.Enums.CardTypes.CustomMonster: return "Custom Monster";
                case Card.Enums.CardTypes.CustomSpellTrap: return "Custom Spell/Trap";
                default: return type.ToString();
            }
        }

        public static bool IsCustom(this Card.Enums.CardTypes type) => SeriesCompatibilities.CustomTypes.Contains(type);
        public static string GetHelpLink(this Card.Enums.CardTypes type)
        {
            switch (type)
            {
                case Card.Enums.CardTypes.Evolute: 
                case Card.Enums.CardTypes.EvoluteConjoint: return "https://www.deviantart.com/superblooman/art/Evolute-Monsters-and-Evolute-Summoning-Full-Guide-773376126";
                case Card.Enums.CardTypes.Criplify:
                case Card.Enums.CardTypes.King: return "https://www.deviantart.com/dragonrikazangetsu/art/King-Card-Rulings-819663785";
                case Card.Enums.CardTypes.Phasm: return "https://www.deviantart.com/batmed/art/Phasm-cards-Template-Rules-505193539";
                default: return "";
            }
        }

        private static string Cleaned(this string toClean) => toClean.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("+", "").Replace("-", "").Replace("/", "");

        public static Card.Enums.CardTypes ToCardType(this string type) => Enum.TryParse(type.Cleaned(), out Card.Enums.CardTypes t) ? t : Card.Enums.CardTypes.Normal;
        public static Card.Enums.Attributes ToAttribute(this string attribute) => Enum.TryParse(attribute.Cleaned(), out Card.Enums.Attributes t) ? t : Card.Enums.Attributes.Dark;
        public static Card.Enums.Symbols ToSymbol(this string symbol) => Enum.TryParse(symbol.Cleaned(), out Card.Enums.Symbols t) ? t : Card.Enums.Symbols.Normal;
        public static Card.Enums.Foil ToFoil(this string foil) => Enum.TryParse(foil.Cleaned(), out Card.Enums.Foil t) ? t : Card.Enums.Foil.None;

        public static bool In<T>(this T value, params T[] values) => values.Contains(value);
    }
}

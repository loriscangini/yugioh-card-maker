﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker
{
    /// <summary>
    /// Logica di interazione per ScriptWindow.xaml
    /// </summary>
    public partial class ScriptWindow : Window
    {
        private string Passcode { get; }

        public ScriptWindow(String name, String passcode, CardTypes type)
        {
            InitializeComponent();
            
            Passcode = passcode.TrimStart('0', ' ');

            script.Text = String.Join(Environment.NewLine,
                $"--{name}",
                $"function c{Passcode}.initial_effect(c)",
                "\t",
                "end");

            using (Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("YuGiOhCardMaker.res.Lua-Mode.xshd"))
            {
                using (XmlTextReader reader = new XmlTextReader(s))
                {
                    script.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                    help.SyntaxHighlighting = script.SyntaxHighlighting;
                }
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog() { FileName = $"c{Passcode}.lua", Filter = "YgoPro script | *.lua" };
            if (dialog.ShowDialog() == true)
            {
                using (StreamWriter outputFile = new StreamWriter(dialog.FileName))
                    outputFile.WriteLine(script.Text);
            }
        }
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog() { Filter = "YgoPro script | *.lua" };
            
            if (dialog.ShowDialog() == true)
            {
                using (StreamReader stream = new StreamReader(dialog.FileName))
                    script.Text = stream.ReadToEnd();
            }
        }

        private void NewEffect_Click(object sender, RoutedEventArgs e)
        {
            String effect = $"e{effectNumber.Value}";

            help.Text = String.Join(Environment.NewLine,
                $"local {effect} = Effect.CreateEffect(c)",
                $"{effect}:SetDescription(aux.Stringid({Passcode},0))",
                $"{effect}:SetCategory(CATEGORY_***)",
                $"{effect}:SetType(EFFECT_TYPE_***)",
                $"{effect}:SetProperty(EFFECT_FLAG_***)",
                $"{effect}:SetCode(EVENT_***)",
                $"{effect}:SetRange(LOCATION_***)",
                $"{effect}:SetCountLimit(*)",
                $"{effect}:SetHintTiming(TIMING_***)",
                $"{effect}:SetCondition(c{Passcode}.condition)",
                $"{effect}:SetCost(c{Passcode}.cost)",
                $"{effect}:SetTarget(c{Passcode}.target)",
                $"{effect}:SetOperation(c{Passcode}.operation)",
                $"c:RegisterEffect({effect})");
        }

        private void Condition_Click(object sender, RoutedEventArgs e)
        {
            help.Text = String.Join(Environment.NewLine,
                $"function c{Passcode}.condition(e,tp,eg,ep,ev,re,r,rp)",
                "\t",
                "end");
        }

        private void Cost_Click(object sender, RoutedEventArgs e)
        {
            help.Text = String.Join(Environment.NewLine,
                $"function c{Passcode}.cost(e,tp,eg,ep,ev,re,r,rp,chk)",
                "\t",
                "end");
        }

        private void Target_Click(object sender, RoutedEventArgs e)
        {

            help.Text = String.Join(Environment.NewLine,
                $"function c{Passcode}.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)",
                "\t",
                "end");
        }

        private void Operation_Click(object sender, RoutedEventArgs e)
        {

            help.Text = String.Join(Environment.NewLine,
                $"function c{Passcode}.operation(e,tp,eg,ep,ev,re,r,rp)",
                "\t",
                "end");
        }

        private void Tutorial_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.ygopro.co/Forum/tabid/95/g/posts/t/39580/AlphaKretin-s-Lua-Tutorials#post179635");
        }
    }
}

﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using WPF.ImageEffects;
using YuGiOhCardMaker.Managers;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Card card;
        private readonly Manager manager;
        private readonly TextEditor textEditor;

        string customFrame = null;
        string customStar = null;
        string customAttribute = null;
        string customSymbol = null;

        public MainWindow()
        {
            card = new Card();
            manager = new Manager(card);
            textEditor = new TextEditor(manager.Visibility, manager.Color) { IsEnabled = false };

            InitializeComponent();

            Series_SelectionChanged(cmbSeries, null);

            chooserDark.Tag = Attributes.Dark;
            chooserDivine.Tag = Attributes.Divine;
            chooserEarth.Tag = Attributes.Earth;
            chooserFire.Tag = Attributes.Fire;
            chooserLight.Tag = Attributes.Light;
            chooserWater.Tag = Attributes.Water;
            chooserWind.Tag = Attributes.Wind;
            chooserLaugh.Tag = Attributes.Laugh;
            chooserSpell.Tag = Attributes.Spell;
            chooserTrap.Tag = Attributes.Trap;
            chooserTrapSpell.Tag = Attributes.TrapSpell;
            chooserVirus.Tag = Attributes.Virus;
            chooserKing.Tag = Attributes.King;

            chooserContinuous.Tag = Symbols.Continuous;
            chooserCounter.Tag = Symbols.Counter;
            chooserEquip.Tag = Symbols.Equip;
            chooserField.Tag = Symbols.Field;
            chooserQuick.Tag = Symbols.QuickPlay;
            chooserRitual.Tag = Symbols.Ritual;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            textEditor.Owner = this;
            textEditor.Show();
        }

        private void Series_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbType == null) return;

            string selected = cmbType.SelectedItem?.ToString();

            card.Series = cmbSeries.SelectedItem.ToString();
            SetCompatibleTypes(selected);

            SetATKDEFEffect();

            if (card.Series.In(Series.Rush, Series.ForKids)) radNone.IsChecked = true;

            UpdateImages();
            UpdateVisibilities();
            UpdatePositions();
            UpdateEnabled();
            UpdateFonts();
        }

        private void SetATKDEFEffect()
        {
            if (card.Series == Series.Rush)
            {
                ATK.Effect = new DropShadowEffect() { ShadowDepth = 0, BlurRadius = 5, Color = Colors.Black };
                DEF.Effect = new DropShadowEffect() { ShadowDepth = 0, BlurRadius = 5, Color = Colors.Black };
                MaximumATK.Effect = new DropShadowEffect() { ShadowDepth = 0, BlurRadius = 5, Color = Colors.Black };
            }
            else
            {
                ATK.Effect = null;
                DEF.Effect = null;
                MaximumATK.Effect = null;
            }
        }

        private void SetCompatibleTypes(string selected)
        {
            cmbType.Items.Clear();
            card.GetCompatibleCardTypes().ToList().ForEach(list =>
            {
                if (list.Count() > 0)
                {
                    list.ToList().ForEach(t => cmbType.Items.Add(t.Prettify()));
                    cmbType.Items.Add(new Separator());
                }
            });
            if (cmbType.Items.Count > 0)
                cmbType.Items.RemoveAt(cmbType.Items.Count - 1);
            if (!string.IsNullOrEmpty(selected) && cmbType.Items.OfType<string>().Contains(selected))
                cmbType.SelectedItem = selected;
            else
                cmbType.SelectedIndex = 0;
        }


        private void CardType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbType.SelectedItem == null) return;

            UpdateType();
            manager.Image.SetFrame(imgCardBack);
            manager.Image.SetLegend(imgLegend);

            UpdatePositions();
            UpdateVisibilities();
            UpdateEnabled();
            UpdateFonts();
        
            ChangeEffectFont();
            ChangeActiveAttribute();
            ChangeColors();
            Passcode_KeyDown(null, null);
            SetFullManga();

            if (CustomCardHelp != null)
                CustomCardHelp.Visibility = card.CardType.IsCustom() && !string.IsNullOrEmpty(card.CardType.GetHelpLink()) ? Visibility.Visible : Visibility.Collapsed;
        }


        #region parts positions
        private void ATK_DEF_Link_SizeChanged(object sender, SizeChangedEventArgs e) => manager.Position.SetATKDEFLink(ATK, ATKLabel, DEF, DEFLabel, linkNumber, LinkLabel, MaximumATK, MaximumATKLabel);

        private void Passcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (edition != null)
                manager.Position.SetPasscodeEdition(passcode, edition);
        }
        #endregion

        private void SetFullManga()
        {
            if (card.IsOriginalManga)
            {
                name.MinWidth = 473;

                radNone.IsChecked = true;
            }
            else
            {
                name.MinWidth = 412;
            }
        }

        private void UpdateVisibilities()
        {
            manager.Visibility.SetName(name);
            manager.Visibility.SetCustom(customOptions);
            manager.Visibility.SetAttribute(imgAttribute);
            UpdateStarType();
            manager.Visibility.SetLegend(imgLegend);
            manager.Visibility.SetKingHierarchy(KingHierarchy);
            manager.Visibility.SetLinkArrows(linkArrows);
            manager.Visibility.SetCriplify(CriplifyFactor);
            manager.Visibility.SetTypeAbilityLine(typeAbility);
            manager.Visibility.SetEffect(effect);
            manager.Visibility.SetBottomEffect(BottomEffect);
            manager.Visibility.SetATKDEFLink(ATK, ATKLabel, RushATK, DEF, DEFLabel, RushDEF, linkNumber, LinkLabel, MaximumATKBox, MaximumATK, MaximumATKLabel, RushMaximumATK);
            manager.Visibility.SetCardNumber(cardNumber);
            manager.Visibility.SetPasscode(passcode);
            manager.Visibility.SetCopyright(copyright);
            manager.Visibility.SetBorder(Border);
        }

        private void UpdateStarType()
        {
            manager.Visibility.SetStars(stars, Evolute1, Evolute2, vigor, rushLevel, customStar);

            Stars_MouseLeave(stars, null);
        }


        private void UpdateEnabled()
        {
            if (chkGoldRare == null) return;

            manager.Enabled.SetSpeedSpellCheckBox(chkSpeedSpell);
            manager.Enabled.SetGoldPlatinumCheckBoxes(chkGoldRare, chkPlatinumRare);
            manager.Enabled.SetPendulumCheckBoxes(chkPendulum, chkMaster, groupPendulumMasterSizes);
            manager.Enabled.SetMaximumCheckBox(chkMaximum);
            manager.Enabled.SetFullCheckBox(chkFull);
            manager.Enabled.SetSmallImageCheckBox(chkSmallImage);
            manager.Enabled.SetCustomCheckBoxes(customHasATK, customHasDEF, customHasLink);
            PendulumBoxSize_ValueChanged(sldPendulumBoxSize, null);
        }

        private void UpdateFonts()
        {
            manager.Color.SetEffect(effect, BottomEffect);
            manager.Font.SetATKDEFLink(ATK, ATKLabel, DEF, DEFLabel, MaximumATK, linkNumber, LinkLabel, MaximumATK, MaximumATKLabel, RushATK, RushDEF, RushMaximumATK);
            manager.Color.SetRushLevel(levelOutline);

            manager.Font.SetRushLevel(levelOutline, level);
        }

        private void UpdatePositions()
        {
            manager.Position.SetName(name, nameOutline);
            manager.Position.SetAttribute(imgAttribute);
            manager.Position.SetStars(stars);
            manager.Position.SetEvolute(Evolute1);
            manager.Position.SetLinkArrows(linkArrows);
            manager.Position.SetArtwork(imgArtwork, fakePendulumArtwork);
            manager.Position.SetLegend(imgLegend);
            manager.Position.SetTypeAbilityLine(typeAbility);
            manager.Position.SetEffectBox(effect);
            manager.Position.SetBottomEffect(BottomEffect);
            manager.Position.SetATKDEFLink(ATK, ATKLabel, DEF, DEFLabel, linkNumber, LinkLabel, MaximumATK, MaximumATKLabel);
            manager.Position.SetCardNumber(cardNumber);
            manager.Position.SetPasscodeEdition(passcode, edition);
            manager.Position.SetAttributeChooser(attributeChooser);
        }

        private void UpdateImages()
        {
            manager.Image.SetBorder(Border);
            manager.Image.SetLegend(imgLegend);

            if (chkGhostRare != null) SetGhostImage(false);

            ChangeAttributesImages();
            ChangeSpellTrapIconsImages();
        }

        private void ChangeActiveAttribute()
        {
            if (card.CardType.In(CardTypes.Spell, CardTypes.LinkSpell))
                card.Attribute = Attributes.Spell;
            else if (card.CardType.In(CardTypes.Trap, CardTypes.LinkTrap))
                card.Attribute = Attributes.Trap;
            else if (card.CardType.In(CardTypes.TrapSpell))
                card.Attribute = Attributes.TrapSpell;
            else if (card.CardType.In(CardTypes.Virus))
                card.Attribute = Attributes.Virus;
            else if (card.CardType.In(CardTypes.King))
                card.Attribute = Attributes.King;
            else if (card.Attribute.In(Attributes.Spell, Attributes.Trap, Attributes.TrapSpell, Attributes.Virus, Attributes.King))
                card.Attribute = Attributes.Dark;

            manager.Image.SetAttribute(imgAttribute, customAttribute);
        }

        #region TypeAbility Line
        private void ResetTypeAbility(IEnumerable<string> abilities)
        {
            Paragraph p = new Paragraph();
            p.Inlines.Add(new Run(card.IsOriginalManga ? "(" : "["));
            p.Inlines.Add(new Run(card.Type));
            foreach (string ability in abilities)
            {
                p.Inlines.Add(new Run("/"));
                p.Inlines.Add(new Run(ability));
            }
            p.Inlines.Add(new Run(card.IsOriginalManga ? ")" : "]"));
            typeAbility.Document.Blocks.Clear();
            typeAbility.Document.Blocks.Add(p);
        }

        private IEnumerable<string> GetTypeAbilities() => typeAbility.Text().Trim('[', ']', '(', ')', ' ').Split('/');
        private void UpdateType()
        {
            CardTypes oldType = card.CardType;

            card.CardType = cmbType.SelectedItem.ToString().ToCardType();

            card.Abilities = new HashSet<string>(GetTypeAbilities().Skip(1));
            if (card.IsPendulum) card.Abilities.Add(Abilities.Pendulum.ToNiceString());
            else card.Abilities.Remove(Abilities.Pendulum.ToNiceString());
            if (card.IsDeckMaster) card.Abilities.Add(Abilities.DeckMaster.ToNiceString());
            else card.Abilities.Remove(Abilities.DeckMaster.ToNiceString());
            if (card.IsMaximum) card.Abilities.Add(Abilities.Maximum.ToNiceString());
            else card.Abilities.Remove(Abilities.Maximum.ToNiceString());
            card.UpdateAbilities(oldType);
            if (card.CardType.In(CardTypes.Spell, CardTypes.Trap, CardTypes.TrapSpell, CardTypes.LinkSpell, CardTypes.LinkTrap, CardTypes.LinkTrapSpell, CardTypes.Virus, CardTypes.King))
                card.Type = card.CardType.TypeAbilityString();

            ResetTypeAbility(card.SortedAbilities);
        }
        #endregion

        private void ChangeEffectFont()
        {
            manager.Font.SetEffect(effect, BottomEffect, pendulumEffect);
        }

        private void ChangeColors()
        {
            manager.Color.SetName(name);
            manager.Color.SetTypeAbility(typeAbility);
            manager.Color.SetCardNumber(cardNumber);
            manager.Color.SetPasscode(passcode);
            manager.Color.SetEditions(limitedEdition, firstEdition, terminalEdition);
            manager.Color.SetCopyright(copyright);
        }

        #region foils and overlays
        private void CmbFoil_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            card.Foil = (cmbFoil.SelectedItem as ComboBoxItem).Content.ToString().ToFoil();

            manager.Visibility.SetFoil(Foil);
            manager.Image.SetFoil(Foil);
            manager.Position.SetFoil(Foil, imgArtwork);
        }

        private void GoldPlatinumChecked(object sender, RoutedEventArgs e)
        {
            card.IsGoldRare = chkGoldRare.IsChecked == true;
            card.IsPlatinumRare = chkPlatinumRare.IsChecked == true;
            
            manager.Image.SetLinkArrows(arrowTopLeft, arrowTop, arrowTopRight, arrowLeft, arrowRight, arrowBottomLeft, arrowBottom, arrowBottomRight);

            manager.Visibility.SetGoldPlatinum(GoldParts);
            manager.Image.SetBorder(Border);
            manager.Image.SetGoldPlatinum(GoldParts);
            manager.Enabled.SetGoldPlatinumCheckBoxes(chkGoldRare, chkPlatinumRare);
        }

        private void SpeedDuel_Checked(object sender, RoutedEventArgs e)
        {
            card.IsSpeedDuel = chkSpeedDuel.IsChecked == true;
            manager.Visibility.SetSpeedDuel(SpeedDuelOverlay);
        }
        private void SpeedSpell_Checked(object sender, RoutedEventArgs e)
        {
            card.IsSpeedSpell = chkSpeedSpell.IsChecked == true;
            manager.Visibility.SetSpeedSpell(SpeedSpellOverlay);
        }

        private void Legend_Checked(object sender, RoutedEventArgs e)
        {
            card.IsLegend = chkLegend.IsChecked == true;
            manager.Visibility.SetLegend(imgLegend);
        }
        #endregion

        private void PendulumOrMaster_Checked(object sender, RoutedEventArgs e)
        {
            card.IsPendulum = chkPendulum.IsChecked.GetValueOrDefault(false);
            card.IsDeckMaster = chkMaster.IsChecked.GetValueOrDefault(false);

            UpdateEnabled();

            manager.Position.SetArtwork(imgArtwork, fakePendulumArtwork);
            manager.Position.SetLinkArrows(linkArrows);
            manager.Position.SetPasscodeEdition(passcode, edition);
            manager.Position.SetCardNumber(cardNumber);
            ChangeColors();
            SetBackHeight();
            UpdateType();
            PendulumBoxSize_ValueChanged(sldPendulumBoxSize, null);
            CmbFoil_SelectionChanged(cmbFoil, null);
            GoldPlatinumChecked(chkGoldRare, null);
        }
        private void Maximum_Checked(object sender, RoutedEventArgs e)
        {
            card.IsMaximum = chkMaximum.IsChecked.GetValueOrDefault(false);

            UpdateEnabled();
            UpdateType();

            manager.Visibility.SetATKDEFLink(ATK, ATKLabel, RushATK, DEF, DEFLabel, RushDEF, linkNumber, LinkLabel, MaximumATKBox, MaximumATK, MaximumATKLabel, RushMaximumATK);
        }

        private void Full_Checked(object sender, RoutedEventArgs e)
        {
            card.IsFullImage = chkFull.IsChecked == true;

            CardType_SelectionChanged(cmbType, null);
            manager.Visibility.SetPendulumDeckMaster(imgPendulum, leftScale, rightScale, pendulumEffect, imgPendulumBar);
            manager.Enabled.SetSmallImageCheckBox(chkSmallImage);
            SetBackHeight();
            CmbFoil_SelectionChanged(cmbFoil, null);
            GoldPlatinumChecked(chkGoldRare, null);
        }

        private void SetBackHeight()
        {
            if (card.IsFullImage && (card.IsDeckMaster || card.IsPendulum))
                imgCardBack.Height = 150;
            else
                imgCardBack.Height = double.NaN;
        }

        #region attributes
        private void ChangeAttributesImages()
        {
            if (chooserDark != null)
            {
                manager.Image.SetAttributes(chooserDark, chooserDivine, chooserEarth, chooserFire, chooserLight, chooserWater, chooserWind, 
                    chooserLaugh, chooserSpell, chooserTrap, chooserTrapSpell, chooserVirus, chooserKing, imgAttribute);
            }
        }

        private void ImgAttribute_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) => attributeChooser.Visibility = Visibility.Visible;

        private void AttributeSelected_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            card.Attribute = (Attributes)(sender as Image).Tag;
            manager.Image.SetAttribute(imgAttribute, customAttribute);

            attributeChooser.Visibility = Visibility.Hidden;
        }
        #endregion

        #region Spell/Trap types
        // private RichTextBox targetControl;

        private void ChangeSpellTrapIconsImages()
        {
            if (chooserContinuous != null)
            {
                manager.Image.SetSymbols(chooserContinuous, chooserCounter, chooserEquip, chooserField, chooserQuick, chooserRitual);
            }
        }
        #endregion

        #region Stars

        private void Stars_MouseEnter(object sender, MouseEventArgs e)
        {
            stars.Children.OfType<Image>().ToList().ForEach(s => s.Visibility = Visibility.Visible);
        }

        private void Stars_MouseLeave(object sender, MouseEventArgs e)
        {
            manager.Visibility.SetStarsOpacity(stars);
            manager.Visibility.HideStars(stars);
        }

        private void Star_MouseDown(object sender, MouseButtonEventArgs e)
        {
            int value = manager.Visibility.GetStarValue(sender as Image);
            card.Stars = (card.Stars == value && card.Stars == 1) ? 0 : value;

            manager.Visibility.SetStars(stars, Evolute1, Evolute2, vigor, rushLevel, customStar);
            manager.Visibility.SetStarsOpacity(stars);

            level.Text = card.Stars.ToString();
        }
        private void Star_MouseEnter(object sender, MouseEventArgs e)
        {
            string baseTooltip = "Click on the desired level/rank to set it.\nIf you have 1 active star, click again on it to set 0 stars.";

            Image star = sender as Image;
            stars.ToolTip = baseTooltip + $"\n\nActive value: {card.Stars} - Selecting value: {manager.Visibility.GetStarValue(star)}";
        }

        private Image evoluteTarget;
        private void Evolute_MouseDown(object sender, MouseButtonEventArgs e)
        {
            evoluteTarget = sender as Image;
            EvoluteNumbers.Visibility = Visibility.Visible;
        }
        private void EvoluteNumber_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            evoluteTarget.Source = (sender as Image).Source;
            evoluteTarget = null;
            EvoluteNumbers.Visibility = Visibility.Hidden;
        }

        #endregion

        #region text sizes and spacing
        private void NameSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (name.LayoutTransform as ScaleTransform).ScaleX = e.NewValue;
            nameOutline.LayoutTransform = name.LayoutTransform;
        }

        private void EffectTextWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (effect.LayoutTransform as ScaleTransform).ScaleX = e.NewValue;
            manager.Position.SetEffectBox(effect);
        }
        private void EffectSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            effect.FontSize = e.NewValue;
            BottomEffect.FontSize = e.NewValue;

            if (sldEffectSpacing != null)
                sldEffectSpacing.Value += e.NewValue - e.OldValue;
        }
        private void EffectSpacing_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            effect.SetValue(Paragraph.LineHeightProperty, e.NewValue);
            BottomEffect.SetValue(Paragraph.LineHeightProperty, e.NewValue);
        }

        private void PendulumEffectTextWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (pendulumEffect.LayoutTransform as ScaleTransform).ScaleX = e.NewValue;
            manager.Position.SetPendulumOrDeckMaster(pendulumEffect, leftScale, rightScale);
        }
        private void PendulumEffectSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            pendulumEffect.FontSize = e.NewValue;

            if (sldPendulumEffectSpacing != null)
                sldPendulumEffectSpacing.Value += e.NewValue - e.OldValue;
        }
        private void PendulumEffectSpacing_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            pendulumEffect.SetValue(Paragraph.LineHeightProperty, e.NewValue);
        }

        private void TypeSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (typeAbility.LayoutTransform as ScaleTransform).ScaleX = e.NewValue;
        }
        #endregion

        #region Link arrows
        private void ToggleArrow(Image arrow)
        {
            int id = linkArrows.Children.OfType<Image>().ToList().IndexOf(arrow);

            card.LinkArrows[id] = !card.LinkArrows[id];

            manager.Image.SetLinkArrows(arrowTopLeft, arrowTop, arrowTopRight, arrowLeft, arrowRight, arrowBottomLeft, arrowBottom, arrowBottomRight);

            linkNumber.Text = card.LinkRating.ToString();
        }

        private void LinkArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) => ToggleArrow((sender as TextBlock).Tag as Image);
        #endregion
        
        private void PendulumBoxSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int value = (int)sldPendulumBoxSize.Value;

            if (!card.IsPendulum && !card.IsDeckMaster)
                value = 1;

            switch (value)
            {
                case 1:
                    card.ActiveBoxSize = BoxSize.Small;
                    break;
                case 2:
                    card.ActiveBoxSize = BoxSize.Medium;
                    break;
                case 3:
                    card.ActiveBoxSize = BoxSize.Large;
                    break;
            }

            manager.Visibility.SetPendulumDeckMaster(imgPendulum, leftScale, rightScale, pendulumEffect, imgPendulumBar);
            manager.Image.SetPendulumDeckMaster(imgPendulum, imgPendulumBar);
            manager.Position.SetTypeAbilityLine(typeAbility);
            manager.Position.SetEffectBox(effect);
            manager.Position.SetPendulumOrDeckMaster(pendulumEffect, leftScale, rightScale);
            manager.Position.SetArtwork(imgArtwork, fakePendulumArtwork);

            if (cmbFoil != null)
                CmbFoil_SelectionChanged(cmbFoil, null);
            if (chkGoldRare != null)
                GoldPlatinumChecked(chkGoldRare, null);
        }

        private void SmallImage_Checked(object sender, RoutedEventArgs e)
        {
            card.IsSmallPendulumImage = chkSmallImage.IsChecked == true;
            manager.Enabled.SetFullCheckBox(chkFull);
            manager.Position.SetArtwork(imgArtwork, fakePendulumArtwork);
            manager.Enabled.SetFullCheckBox(chkFull);
        }

        #region Save/Load Images
        private void SaveImage(FrameworkElement control)
        {
            Rect rect = VisualTreeHelper.GetDescendantBounds(control);

            DrawingVisual dv = new DrawingVisual();

            using (DrawingContext ctx = dv.RenderOpen())
            {
                ctx.DrawRectangle(new VisualBrush(control), null, new Rect(rect.X, rect.Y, rect.Width, rect.Height));
            }

            //double scale = 1.5;
            RenderTargetBitmap rtb = new RenderTargetBitmap(549, 800, 96, 96, PixelFormats.Pbgra32);

            rtb.Render(dv);

            // Make a PNG encoder.
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));

            SaveFileDialog dialog = new SaveFileDialog
            {
                Filter = "png image|*.png",
                FileName = name.Text()
            };

            if (dialog.ShowDialog() == true)
            {
                using (FileStream fs = new FileStream(dialog.FileName, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    encoder.Save(fs);
                }
            }
        }

        private void LoadImage(String filename, bool crop = true)
        {
            try
            {
                LoadImage(new BitmapImage(new Uri(filename)), crop);
            }
            catch { }
        }
        private void LoadImage(BitmapSource image, bool crop = true)
        {
            ImageCropWindow ic = new ImageCropWindow(image);
            if (crop)
                ic.ShowDialog();

            ImageSource cropped = ic.CroppedImage;

            imgArtwork.Source = cropped;
            SetGhostImage(true);
        }

        private void SaveAsImage_Click(object sender, RoutedEventArgs e)
        {
            Foil.SetValue(Panel.ZIndexProperty, 1);

            cmbType.Focus();
            SaveImage(grdCard);

            Foil.SetValue(Panel.ZIndexProperty, 0);
        }
        private void SaveAsImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) => SaveAsImage_Click(null, null);

        private void ImgArtwork_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "images|*.jpg;*.png;*.bmp;*.gif|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == true)
                LoadImage(dialog.FileName);
        }
        private void Artwork_Drop(object sender, DragEventArgs e)
        {
            try
            {
                string file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                if (file.EndsWith(".ycp"))
                    LoadProject(file);
                else
                    LoadImage(file);
            }
            catch { }

        }
        private void PasteArtwork_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsImage())
            {
                LoadImage(Clipboard.GetImage());
            }
        }
        #endregion

        #region Save/Load projects
        private void LoadProject(string filename)
        {
            using (StreamReader sr = new StreamReader(filename))
            {
                void Toggle(Image arrow, bool value)
                {
                    if ((value && arrow.Visibility == Visibility.Hidden) || (!value && arrow.Visibility == Visibility.Visible))
                        ToggleArrow(arrow);
                }

                List<string> lines = sr.ReadToEnd().Split('\n').ToList();
                lines.ForEach(line =>
                {
                    string[] parts = line.Split(new char[] { '=' }, 2);
                    switch (parts[0])
                    {
                        case "Name": parts[1].ToRichTextBox(name); break;
                        case "NameOutline": parts[1].ToOutline(nameOutline); break;
                        case "Name Size": sldNameSize.Value = double.Parse(parts[1]); break;
                        case "Series": cmbSeries.SelectedItem = cmbSeries.Items.OfType<string>().First(item => item == parts[1]); break;
                        case "CardType": cmbType.SelectedItem = cmbType.Items.OfType<string>().First(item => item.ToCardType().ToString() == parts[1]); break;
                        case "Pendulum": chkPendulum.IsChecked = bool.Parse(parts[1]); break;
                        case "Pendulum Size": sldPendulumBoxSize.Value = int.Parse(parts[1]); break;
                        case "Master": chkMaster.IsChecked = bool.Parse(parts[1]); break;
                        case "Full": chkFull.IsChecked = bool.Parse(parts[1]); break;
                        case "SmallImage": chkSmallImage.IsChecked = bool.Parse(parts[1]); break;
                        case "Legend": chkLegend.IsChecked = bool.Parse(parts[1]); break;
                        case "CustomAttribute": customAttribute = parts[1]; break;
                        case "Attribute": card.Attribute = parts[1].ToAttribute(); manager.Image.SetAttribute(imgAttribute, customAttribute); break;
                        case "Level": card.Stars = int.Parse(parts[1]); manager.Visibility.SetStarsOpacity(stars); manager.Visibility.HideStars(stars); break;
                        case "Evolute1": Evolute1.Source = new BitmapImage(new Uri(parts[1])); break;
                        case "Evolute2": Evolute2.Source = new BitmapImage(new Uri(parts[1])); break;
                        // case "TopType": TopType.Text = parts[1]; break;
                        case "KingHierarchy": KingHierarchy.Text = parts[1]; break;
                        // case "Symbol": targetST = SpellTrapSymbol; ToggleSymbol(parts[1].ToSymbol().GetImage(card.Series), parts[1].ToSymbol()); break;
                        // case "Symbol2": targetST = SpellTrapSymbol2; ToggleSymbol(parts[1].ToSymbol().GetImage(card.Series), parts[1].ToSymbol()); break;
                        case "Artwork": LoadImage(parts[1], false); break;
                        case "ArrowTL": Toggle(arrowTopLeft, bool.Parse(parts[1])); break;
                        case "ArrowT": Toggle(arrowTop, bool.Parse(parts[1])); break;
                        case "ArrowTR": Toggle(arrowTopRight, bool.Parse(parts[1])); break;
                        case "ArrowL": Toggle(arrowLeft, bool.Parse(parts[1])); break;
                        case "ArrowR": Toggle(arrowRight, bool.Parse(parts[1])); break;
                        case "ArrowBL": Toggle(arrowBottomLeft, bool.Parse(parts[1])); break;
                        case "ArrowB": Toggle(arrowBottom, bool.Parse(parts[1])); break;
                        case "ArrowBR": Toggle(arrowBottomRight, bool.Parse(parts[1])); break;
                        case "Scales": string[] scales = parts[1].Split(','); leftScale.Text = scales[0]; rightScale.Text = scales[1]; break;
                        case "CriplifyFactor": CriplifyFactor.Text = parts[1]; break;
                        case "TypeAbilities": typeAbility.Document.Blocks.Clear(); typeAbility.Document.Blocks.Add(new Paragraph(new Run(parts[1]))); break;
                        case "Type Size": sldTypeSize.Value = double.Parse(parts[1]); break;
                        case "Effect": parts[1].ToRichTextBox(effect); break;
                        case "Pendulum Effect": parts[1].ToRichTextBox(pendulumEffect); break;
                        case "ATK": parts[1].ToTextBox(ATK); break;
                        case "ATKOutline": parts[1].ToOutline(RushATK); break;
                        case "DEF": parts[1].ToTextBox(DEF); break;
                        case "DEFOutline": parts[1].ToOutline(RushDEF); break;
                        case "CardNumber": parts[1].ToRichTextBox(cardNumber); break;
                        case "Passcode": parts[1].ToRichTextBox(passcode); break;
                        case "Edition":
                            switch ((Editions)Enum.Parse(typeof(Editions), parts[1]))
                            {
                                case Editions.Unlimited: radUnlimited.IsChecked = true; break;
                                case Editions.First: radFirst.IsChecked = true; break;
                                case Editions.Limited: radLimited.IsChecked = true; break;
                            }
                            break;
                        case "Copyright": parts[1].ToRichTextBox(copyright); break;
                        case "Foil": cmbFoil.SelectedItem = cmbFoil.Items.OfType<ComboBoxItem>().First(item => item.Content.ToString() == parts[1]); break;
                        case "CustomFrame": customFrame = parts[1]; manager.Image.SetFrame(imgCardBack, customFrame); break;
                        case "CustomStar": customStar = parts[1]; UpdateStarType(); break;
                        case "CustomStarPosition": customStarPosition.Value = double.Parse(parts[1]); break;
                        case "CustomHasATK": customHasATK.IsChecked = bool.Parse(parts[1]); break;
                        case "CustomHasDEF": customHasDEF.IsChecked = bool.Parse(parts[1]); break;
                    }
                });
            }
        }

        private void SaveAsProject_Click(object sender, RoutedEventArgs e)
        {
            string toSave =
                $"Name={name.ToXml()}\n" +
                $"NameOutline={nameOutline.ToXml()}\n" +
                $"Name Size={(name.LayoutTransform as ScaleTransform).ScaleX}\n" +
                $"Series={card.Series}\n" +
                $"CardType={card.CardType}\n" +
                $"Pendulum={card.IsPendulum}\n" +
                $"Pendulum Size={sldPendulumBoxSize.Value}\n" +
                $"Master={card.IsDeckMaster}\n" +
                $"Full={card.IsFullImage}\n" +
                $"SmallImage={card.IsSmallPendulumImage}\n" +
                $"Legend={card.IsLegend}\n" +
                $"CustomAttribute={customAttribute}\n" +
                $"Attribute={card.Attribute}\n" +
                $"Level={card.Stars}\n" +
                $"Evolute1={Evolute1.Source}\n" +
                $"Evolute2={Evolute2.Source}\n" +
                $"KingHierarchy={KingHierarchy.Text}\n" +
                $"Artwork={imgArtwork.Source}\n" +
                $"ArrowTL={card.LinkArrows[0]}\n" +
                $"ArrowT={card.LinkArrows[1]}\n" +
                $"ArrowTR={card.LinkArrows[2]}\n" +
                $"ArrowL={card.LinkArrows[3]}\n" +
                $"ArrowR={card.LinkArrows[4]}\n" +
                $"ArrowBL={card.LinkArrows[5]}\n" +
                $"ArrowB={card.LinkArrows[6]}\n" +
                $"ArrowBR={card.LinkArrows[7]}\n" +
                $"Scales={leftScale.Text},{rightScale.Text}\n" +
                $"CriplifyFactor={CriplifyFactor.Text}\n" +
                $"TypeAbilities={typeAbility.Text()}\n" +
                $"Type Size={(typeAbility.LayoutTransform as ScaleTransform).ScaleX}\n" +
                $"Effect={effect.ToXml()}\n" +
                $"Pendulum Effect={pendulumEffect.ToXml()}\n" +
                $"ATK={ATK.ToXml()}\n" +
                $"ATKOutline={RushATK.ToXml()}\n" +
                $"DEF={DEF.ToXml()}\n" +
                $"DEFOutline={RushDEF.ToXml()}\n" +
                $"CardNumber={cardNumber.ToXml()}\n" +
                $"Passcode={passcode.ToXml()}\n" +
                $"Edition={card.Edition}\n" +
                $"Copyright={copyright.ToXml()}\n" +
                $"Foil={(cmbFoil.SelectedValue as ComboBoxItem).Content}\n" +
                $"CustomFrame={customFrame}\n" +
                $"CustomStar={customStar}\n" +
                $"CustomStarPosition={customStarPosition.Value}\n" +
                $"CustomHasATK={customHasATK.IsChecked}\n" +
                $"CustomHasDEF={customHasDEF.IsChecked}\n";

            SaveFileDialog dialog = new SaveFileDialog
            {
                Filter = "Yu-Gi-Oh! Card Project|*.ycp",
                FileName = name.Text()
            };
            if (dialog.ShowDialog() == true)
            {
                using (StreamWriter outputFile = new StreamWriter(dialog.FileName))
                {
                    outputFile.WriteLine(toSave);
                }
            }
        }
        private void SaveAsProject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) => SaveAsProject_Click(null, null);

        private void LoadProject_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "Yu-Gi-Oh! Card Project|*.ycp"
            };
            if (dialog.ShowDialog() == true)
                LoadProject(dialog.FileName);
        }
        #endregion

        #region Editions
        private void UnlimitedEdition_Cheked(object sender, RoutedEventArgs e)
        {
            card.Edition = Editions.Unlimited;
            manager.Visibility.SetEdition(firstEdition, limitedEdition, terminalEdition, sticker);
        }
        private void FirstEdition_Checked(object sender, RoutedEventArgs e)
        {
            card.Edition = Editions.First;
            manager.Visibility.SetEdition(firstEdition, limitedEdition, terminalEdition, sticker);
        }
        private void LimitedEdition_Cheked(object sender, RoutedEventArgs e)
        {
            card.Edition = Editions.Limited;
            manager.Visibility.SetEdition(firstEdition, limitedEdition, terminalEdition, sticker);
        }
        private void TerminalEdition_Cheked(object sender, RoutedEventArgs e)
        {
            card.Edition = Editions.Terminal;
            manager.Visibility.SetEdition(firstEdition, limitedEdition, terminalEdition, sticker);
        }
        private void NoneEdition_Cheked(object sender, RoutedEventArgs e)
        {
            card.Edition = Editions.None;
            manager.Visibility.SetEdition(firstEdition, limitedEdition, terminalEdition, sticker);
        }
        #endregion

        [Obsolete]
        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {
            Paragraph p = new Paragraph();
            if (name.Document.Blocks.FirstBlock is Paragraph)
            {
                (name.Document.Blocks.FirstBlock as Paragraph).Inlines.OfType<Run>().ToList().ForEach(r =>
                {
                    p.Inlines.Add(new Run(r.Text)
                    {
                        FontFamily = r.FontFamily,
                        FontSize = r.FontSize,
                        FontWeight = r.FontWeight,
                        FontStretch = r.FontStretch,
                        FontStyle = r.FontStyle
                    });
                });
            }
            nameOutline.Paragraph = p;

            FormattedText text = new FormattedText(name.Text(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                new Typeface(name.FontFamily, name.FontStyle, name.FontWeight, name.FontStretch),
                name.FontSize, name.Foreground)
            {
                Trimming = TextTrimming.None
            };

            ScaleTransform transform = name.LayoutTransform as ScaleTransform;
            if (text.Width * transform.ScaleX > name.MinWidth)
            {
                sldNameSize.Value -= sldNameSize.SmallChange;
                Name_TextChanged(sender, e);
            }
            else if (transform.ScaleX < 1 && text.Width * (transform.ScaleX + sldNameSize.SmallChange) < name.MinWidth)
            {
                sldNameSize.Value += sldNameSize.SmallChange;
                Name_TextChanged(sender, e);
            }
        }

        private void ATK_TextChanged(object sender, TextChangedEventArgs e) => UpdateFonts();

        private void CustomCardHelp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(card.CardType.GetHelpLink());
            }
            catch
            {
                System.Windows.MessageBox.Show("Cannot open " + card.CardType.GetHelpLink(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CreateScript_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(passcode.Text()))
                System.Windows.MessageBox.Show("The card should have a passcode.", "Passcode required", MessageBoxButton.OK, MessageBoxImage.Warning);
            
            new ScriptWindow(name.Text(), passcode.Text(), card.CardType).Show();
        }

        #region Ghost rarity
        ImageSource original;
        private void ChkGhostRare_Checked(object sender, RoutedEventArgs e)
        {
            SetGhostImage(card.Series != Series.Manga || original == null);
        }
        void SetGhostImage(bool setOriginal) {
            if (chkGhostRare.IsChecked == true || card.Series == Series.Manga)
            {
                if (setOriginal) original = imgArtwork.Source;
                imgArtwork.Source = ToGrayScaleImage(imgArtwork.Source);
                imgArtwork.Effect = new BrightnessContrastEffect() { Brightness = sldBrightness.Value, Contrast = sldContrast.Value };
                sldBrightness.Visibility = Visibility.Visible;
            }
            else if (original != null)
            {
                imgArtwork.Source = original;
                original = null;
                imgArtwork.Effect = null;
                if (sldBrightness != null)
                    sldBrightness.Visibility = Visibility.Collapsed;
            }
        }

        private ImageSource ToGrayScaleImage(ImageSource source)
        {
            FormatConvertedBitmap grayBitmap = new FormatConvertedBitmap();
            grayBitmap.BeginInit();

            grayBitmap.Source = source as BitmapSource;
            grayBitmap.DestinationFormat = PixelFormats.Gray32Float;
            grayBitmap.EndInit();

            return grayBitmap;
        }

        private void SldContrast_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (imgArtwork.Effect is BrightnessContrastEffect)
                (imgArtwork.Effect as BrightnessContrastEffect).Contrast = e.NewValue;
        }
        #endregion

        private void SldBrightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (imgArtwork.Effect is BrightnessContrastEffect)
                (imgArtwork.Effect as BrightnessContrastEffect).Brightness = e.NewValue;
        }

        T GetFocusedElement<T>() where T : class, IInputElement => FocusManager.GetFocusedElement(this) as T;
        OutlineTextControl GetOutline(IInputElement box) 
        {
            if (box == name) return nameOutline;
            else if (box == ATK && card.Series == Series.Rush) return RushATK;
            else if (box == DEF && card.Series == Series.Rush) return RushDEF;
            else if (box == MaximumATK && card.Series == Series.Rush) return RushMaximumATK;
            else if (box == level && card.Series == Series.Rush) return levelOutline;
            else return null;
        }

        private void EffectSymbol_Click(object sender, RoutedEventArgs e)
        {
            RichTextBox box = GetFocusedElement<RichTextBox>();
            box.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, Managers.Fonts.DFLeisho4);
            box.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, 40.0);
        }
        private void EffectNameFont_Click(object sender, RoutedEventArgs e)
        {
            RichTextBox box = GetFocusedElement<RichTextBox>();
            box.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, Managers.Fonts.MatrixRegularSmallCaps);
            box.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, 60.0);
        }
        private void NameAtSymbol_Click(object sender, RoutedEventArgs e)
        {
            RichTextBox box = GetFocusedElement<RichTextBox>();
            box.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, Managers.Fonts.SanDiegoStAztecs);
            box.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, 45.0);
        }
        private void AddSymbol(string symbol)
        {
            RichTextBox box = GetFocusedElement<RichTextBox>();
            box.Selection.Text += symbol;
        }
        private void AddNameSymbol(string symbol)
        {
            AddSymbol(symbol);
            EffectSymbol_Click(null, null);
            if (symbol == "@")
                GetFocusedElement<RichTextBox>().Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, Managers.Fonts.SanDiegoStAztecs);
        }
        private void AddSymbol_Click(object sender, RoutedEventArgs e) => AddSymbol((sender as MenuItem).Header.ToString());
        private void AddNameSymbol_Click(object sender, RoutedEventArgs e) => AddNameSymbol((sender as MenuItem).Header.ToString());
        private void AddATKSymbol_Click(object sender, RoutedEventArgs e) => (Keyboard.FocusedElement as TextBox).Text += (sender as MenuItem).Header;

        private void Effect_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                switch (e.Key)
                {
                    case Key.OemPeriod: case Key.Decimal: AddSymbol("●"); break;
                    case Key.OemMinus: case Key.Subtract: AddSymbol("∞"); break;
                    case Key.D0: case Key.NumPad0: AddSymbol("⓪"); break;
                    case Key.D1: case Key.NumPad1: AddSymbol("①"); break;
                    case Key.D2: case Key.NumPad2: AddSymbol("②"); break;
                    case Key.D3: case Key.NumPad3: AddSymbol("③"); break;
                    case Key.D4: case Key.NumPad4: AddSymbol("④"); break;
                    case Key.D5: case Key.NumPad5: AddSymbol("⑤"); break;
                    case Key.D6: case Key.NumPad6: AddSymbol("⑥"); break;
                    case Key.D7: case Key.NumPad7: AddSymbol("⑦"); break;
                    case Key.D8: case Key.NumPad8: AddSymbol("⑧"); break;
                    case Key.D9: case Key.NumPad9: AddSymbol("⑨"); break;
                    case Key.LeftCtrl: case Key.RightCtrl: break;
                    default: break;
                }
            }
        }
        private void Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                switch (e.Key)
                {
                    case Key.S: AddNameSymbol("☆"); break;
                    case Key.OemMinus: case Key.Subtract: AddNameSymbol("∞"); break;
                }
            }
        }
        private void ATK_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                switch (e.Key)
                {
                    case Key.OemMinus: case Key.Subtract: (Keyboard.FocusedElement as TextBox).Text += "∞"; break;
                }
            }
        }

        private void Level_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(level.Text, out int l))
                card.Stars = l;

            manager.Font.SetRushLevel(levelOutline, level);
        }

        private void EffectReset_Click(object sender, RoutedEventArgs e)
        {
            sldEffectTextWidth.Value = 1;
            sldEffectSpacing.Value = 20;
        }
        private void PendulumEffectReset_Click(object sender, RoutedEventArgs e)
        {
            sldPendulumTextWidth.Value = 1;
            sldPendulumEffectSpacing.Value = 20;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            textEditor.Closing -= textEditor.Window_Closing;
            textEditor.Close();
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
            textEditor.IsEnabled = Keyboard.FocusedElement is IInputElement;
        }

        private void RichTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            IInputElement box = GetFocusedElement<IInputElement>();
            textEditor.Update(box as Control, GetOutline(box));
            textEditor.Show();
        }

        private void LoadCustomFrame_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "images|*.jpg;*.png;*.bmp;*.gif|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == true)
            {
                customFrame = dialog.FileName;
                manager.Image.SetFrame(imgCardBack, customFrame);
            }
        }

        private void LoadCustomStar_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "images|*.jpg;*.png;*.bmp;*.gif|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == true)
            {
                customStar = dialog.FileName;
                UpdateStarType();
            }
        }
        private void StarPosition_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // (name.LayoutTransform as ScaleTransform).ScaleX = e.NewValue;
            switch (Math.Round(e.NewValue)) {
                case 0:
                    card.CustomStarPosition = HorizontalAlignment.Left;
                    break;
                case 1:
                    card.CustomStarPosition = HorizontalAlignment.Center;
                    break;
                case 2:
                    card.CustomStarPosition = HorizontalAlignment.Right;
                    break;
            }
            UpdateStarType();
        }
        private void CustomATKDEF_Checked(object sender, RoutedEventArgs e)
        {
            card.CustomHasATK = customHasATK.IsChecked.GetValueOrDefault(true);
            card.CustomHasDEF = customHasDEF?.IsChecked.Value  ?? true;
            card.CustomHasLink = customHasLink?.IsChecked.Value ?? false;
            if (customHasATK != null && customHasDEF != null && customHasLink != null)
                UpdateEnabled();
            UpdateVisibilities();
        }

        private void customAttribute_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "images|*.jpg;*.png;*.bmp;*.gif|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == true)
            {
                customAttribute = dialog.FileName;
                card.Attribute = Attributes.Custom;
                manager.Image.SetAttribute(imgAttribute, customAttribute);
            }

            attributeChooser.Visibility = Visibility.Hidden;
        }
        private void customSymbol_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "images|*.jpg;*.png;*.bmp;*.gif|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == true)
            {
                customSymbol = dialog.FileName;
                SymbolSelected_MouseLeftButtonDown(new Image() { Source = new BitmapImage(new Uri(customSymbol)) }, null);
            }
        }

        private void TypeAbility_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(typeAbility.Text()))
                card.Type = GetTypeAbilities().ElementAt(0);
        }

        private void TypeAbility_GotFocus(object sender, RoutedEventArgs e)
        {
            typeAbilityHelper.Margin = new Thickness(typeAbility.Margin.Left, typeAbility.Margin.Top + 30, typeAbility.Margin.Right, typeAbility.Margin.Bottom);
            typeAbilityHelper.Visibility = Visibility.Visible;
        }

        private void TypeAbility_LostFocus(object sender, RoutedEventArgs e)
        {
            typeAbilityHelper.Visibility = Visibility.Hidden;
        }

        private void typeAbilityValueSelected(object sender, MouseButtonEventArgs e)
        {
            typeAbility.Selection.Text = "";
            typeAbility.CaretPosition.InsertTextInRun((sender as TextBlock).Text);
        }

        private void typeAbilityHelper_GotFocus(object sender, RoutedEventArgs e)
        {
            typeAbilityHelper.Visibility = Visibility.Visible;
        }

        private void typeAbilityHelper_LostFocus(object sender, RoutedEventArgs e)
        {
            typeAbilityHelper.Visibility = Visibility.Hidden;
        }
        private void SymbolSelected_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            new InlineUIContainer(new Image()
            {
                Source = (sender as Image).Source,
                Height = 25,
                Width = 25,
                VerticalAlignment = VerticalAlignment.Bottom
            }, typeAbility.CaretPosition)
            {
                BaselineAlignment = BaselineAlignment.Center
            };
        }
    }
}

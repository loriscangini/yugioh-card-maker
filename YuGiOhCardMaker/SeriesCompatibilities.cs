﻿using System.Collections.Generic;
using System.Linq;
using static YuGiOhCardMaker.Card.Enums;

namespace YuGiOhCardMaker
{
    public static class SeriesCompatibilities
    {
        public static Dictionary<string, List<CardTypes>> RegularMonsters = new Dictionary<string, List<CardTypes>>
        {
            { Series.Manga,
                new List<CardTypes>()
                {
                    CardTypes.Normal,
                    CardTypes.Effect,
                    CardTypes.Xyz,
                    CardTypes.DarkSynchro,
                    CardTypes.OriginalMonster,
                    CardTypes.OriginalXyz,
                    CardTypes.OriginalDarkSynchro,
                }
            },
            { Series.ForKids,
                new List<CardTypes>()
                {
                    CardTypes.Normal,
                    CardTypes.Effect,
                    CardTypes.Ritual,
                    CardTypes.Fusion,
                    CardTypes.Synchro,
                    CardTypes.DarkSynchro,
                    CardTypes.Xyz,
                    CardTypes.Link,
                    CardTypes.Token,
                    CardTypes.Slifer,
                    CardTypes.Obelisk,
                    CardTypes.Ra,
                    CardTypes.RaChant,
                    CardTypes.WickedGod,
                    CardTypes.LegendaryDragon,
                    CardTypes.ZArcFusionSynchroXyz,
                    CardTypes.ZArcFusionSynchro,
                    CardTypes.ZArcFusion,
                }
            },
            { Series.Series9,
                new List<CardTypes>()
                {
                    CardTypes.Normal,
                    CardTypes.Effect,
                    CardTypes.Ritual,
                    CardTypes.Fusion,
                    CardTypes.Synchro,
                    CardTypes.Xyz,
                    CardTypes.Token,
                    CardTypes.WickedGod,
                }
            },
            { Series.Series10,
                new List<CardTypes>()
                {
                    CardTypes.Normal,
                    CardTypes.Effect,
                    CardTypes.Ritual,
                    CardTypes.Fusion,
                    CardTypes.Synchro,
                    CardTypes.DarkSynchro,
                    CardTypes.Xyz,
                    CardTypes.Link,
                    CardTypes.Token,
                    CardTypes.Slifer,
                    CardTypes.Obelisk,
                    CardTypes.Ra,
                    CardTypes.WickedGod,
                    CardTypes.LegendaryDragon,
                    CardTypes.ZArcFusionSynchroXyz,
                    CardTypes.ZArcFusionSynchro,
                    CardTypes.ZArcFusion,
                }
            },
            { Series.Rush,
                new List<CardTypes>()
                {
                    CardTypes.Normal,
                    CardTypes.Effect,
                    CardTypes.Ritual,
                    CardTypes.Fusion,
                    CardTypes.Synchro,
                    CardTypes.DarkSynchro,
                    CardTypes.Xyz,
                    //CardTypes.Link,
                    CardTypes.Token,
                    CardTypes.Slifer,
                    CardTypes.Obelisk,
                    CardTypes.Ra,
                    CardTypes.WickedGod,
                    CardTypes.LegendaryDragon,
                }
            }
        };
        public static Dictionary<string, List<CardTypes>> CustomMonsters = new Dictionary<string, List<CardTypes>>
        {
            { Series.ForKids,
                new List<CardTypes>()
                {
                    CardTypes.CustomMonster
                }
            },
            { Series.Series9,
                new List<CardTypes>()
                {
                    CardTypes.CustomMonster,
                    CardTypes.Evolute,
                    CardTypes.EvoluteConjoint,
                    CardTypes.Criplify,
                    CardTypes.Evolution,
                    CardTypes.Phasm
                }
            },
            { Series.Series10,
                new List<CardTypes>()
                {
                    CardTypes.CustomMonster,
                    CardTypes.Evolution,
                    CardTypes.Phasm
                }
            },
            { Series.Rush,
                new List<CardTypes>()
                {
                    CardTypes.CustomMonster
                }
            },
        };

        public static Dictionary<string, List<CardTypes>> RegularSpellTraps = new Dictionary<string, List<CardTypes>>
        {
            { Series.Manga,
                new List<CardTypes>()
                {
                    CardTypes.Spell,
                    CardTypes.Trap,
                    CardTypes.TrapSpell,
                    CardTypes.Virus,
                    CardTypes.OriginalSpellTrap,
                }
            },
            { Series.ForKids,
                new List<CardTypes>()
                {
                    CardTypes.Spell,
                    CardTypes.Trap
                }
            },
            { Series.Series9,
                new List<CardTypes>()
                {
                    CardTypes.Spell,
                    CardTypes.Trap,
                    CardTypes.TrapSpell,
                    CardTypes.Virus
                }
            },
            { Series.Series10,
                new List<CardTypes>()
                {
                    CardTypes.Spell,
                    CardTypes.Trap,
                    CardTypes.LinkSpell,
                    CardTypes.TrapSpell,
                    CardTypes.Virus
                }
            },
            { Series.Rush,
                new List<CardTypes>()
                {
                    CardTypes.Spell,
                    CardTypes.Trap,
                    CardTypes.TrapSpell,
                    CardTypes.Virus
                }
            }
        };
        public static Dictionary<string, List<CardTypes>> CustomSpellTraps = new Dictionary<string, List<CardTypes>>
        {
            { Series.ForKids,
                new List<CardTypes>()
                {
                    CardTypes.CustomSpellTrap
                }
            },
            { Series.Series9,
                new List<CardTypes>()
                {
                    CardTypes.CustomSpellTrap
                }
            },
            { Series.Series10,
                new List<CardTypes>()
                {
                    CardTypes.CustomSpellTrap,
                    CardTypes.LinkTrap,
                    CardTypes.LinkTrapSpell,
                    CardTypes.King
                }
            },
            { Series.Rush,
                new List<CardTypes>()
                {
                    CardTypes.CustomSpellTrap
                }
            },
        };

        public static Dictionary<string, List<CardTypes>> RegularOthers = new Dictionary<string, List<CardTypes>>
        {
            { Series.Series10,
                new List<CardTypes>()
                {
                    CardTypes.Skill,
                }
            }
        };
        public static Dictionary<string, List<CardTypes>> CustomOthers = new Dictionary<string, List<CardTypes>>
        {
        };

        public static IEnumerable<CardTypes> Monsters => RegularMonsters.Values.Union(CustomMonsters.Values).SelectMany(l => l);

        public static IEnumerable<CardTypes> CustomTypes => CustomMonsters.Values.Union(CustomSpellTraps.Values).Union(CustomOthers.Values).SelectMany(l => l);
    }
}

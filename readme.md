﻿# Yu-Gi-Oh! Card Maker

I proudly present you my Card Creator.
It is a program that lets everyone create their own cards with a small effort.

The only requirement to use the program is .NET Framework 4.8.
Also don't split the .exe from the .dll files.

## Included Series
At the moment, it's possible to create cards using templates for the following series:

* [Manga](https://yugioh.fandom.com/wiki/Card_layout#Manga)
* [Series 9](https://yugioh.fandom.com/wiki/Card_layout#Series_9_layout)
* [Series 10](https://yugioh.fandom.com/wiki/Card_layout#Series_10_layout)
* [Rush Duel](https://yugioh.fandom.com/wiki/Rush_Duel) (Beta support)

## Included Templates

### Official Card Types

These are the TGC/OGC, Anime and Manga templates actually included:

* [Normal monsters](https://yugioh.fandom.com/wiki/Normal_Monster)
* [Effect monsters](https://yugioh.fandom.com/wiki/Effect_Monster)
* [Ritual monsters](https://yugioh.fandom.com/wiki/Ritual_Monster)
* [Fusion monsters](https://yugioh.fandom.com/wiki/Fusion_Monster)
* [Synchro monsters](https://yugioh.fandom.com/wiki/Synchro_Monster)
* [Dark Synchro monsters](https://yugioh.fandom.com/wiki/Dark_Synchro_Monster)
* [Xyz monsters](https://yugioh.fandom.com/wiki/Xyz_Monster)
* [Link monsters](https://yugioh.fandom.com/wiki/Link_Monster)
* [Tokens](https://yugioh.fandom.com/wiki/Monster_Token)
* [Slifer](https://yugioh.fandom.com/wiki/Slifer_the_Sky_Dragon_(original))
* [Obelisk](https://yugioh.fandom.com/wiki/Obelisk_the_Tormentor_(original))
* [Ra](https://yugioh.fandom.com/wiki/The_Winged_Dragon_of_Ra_(original))
* [Wicked God](https://yugioh.fandom.com/wiki/Wicked_God)
* [Legendary Dragon](https://yugioh.fandom.com/wiki/Legendary_Dragon)
* [Z-Arc](https://yugioh.fandom.com/wiki/Supreme_King_Z-ARC_(anime))
* [Spell cards](https://yugioh.fandom.com/wiki/Spell_Card)
* [Trap cards](https://yugioh.fandom.com/wiki/Trap_Card)
* [Link Spell cards](https://yugioh.fandom.com/wiki/Link_Spell_Card)
* [Trap Spell cards](https://yugioh.fandom.com/wiki/Trap_Spell_Card)
* [Virus cards](https://yugioh.fandom.com/wiki/Virus_Card)
* [Skill cards](https://yugioh.fandom.com/wiki/Skill_Card)

It's also possible to create [Pendulum](https://yugioh.fandom.com/wiki/Pendulum_Monster) and [Deck Master](https://yugioh.fandom.com/wiki/Deck_Master) monsters with most of the available templates.

### Custom Card Types

* Custom cards using your own template
* [Evolute monsters](https://www.deviantart.com/superblooman/art/Evolute-Monsters-and-Evolute-Summoning-Full-Guide-773376126) by [SuperBlooman](https://www.deviantart.com/superblooman)
* [Evolute Conjoint monsters](https://www.deviantart.com/superblooman/art/Evolute-Monsters-and-Evolute-Summoning-Full-Guide-773376126) by [SuperBlooman](https://www.deviantart.com/superblooman)
* [Criplify monsters](https://www.deviantart.com/batmed/art/Prism-Cryplify-Monsters-Template-Rules-505172543) by [BatMed](https://www.deviantart.com/batmed)
* [Evolution monsters](https://www.deviantart.com/grezar/art/EVOLUTION-MONSTER-Fire-Dancer-473644880) by [grezar](https://www.deviantart.com/grezar)
* [Phasm monsters](https://www.deviantart.com/batmed/art/Phasm-cards-Template-Rules-505193539) by [BatMed](https://www.deviantart.com/batmed)
* [King cards](https://www.deviantart.com/dragonrikazangetsu/art/King-Card-Rulings-819663785) by [DragonRikaZangetsu](https://www.deviantart.com/dragonrikazangetsu)
* Link Trap cards by [Me](https://www.deviantart.com/loriscangini)
* Link Trap Spell cards by [Me](https://www.deviantart.com/loriscangini)

## Changelog

#### Update 1.1

* Added Rebirth monsters under NeoPhoenixKnight's request.
* Added the possibility to resize the window to let even users with smaller screens to use this program.
* Changed a little bit the way hints where given, hopefully this way will be faster.

#### Update 1.2

* Added the possibility to change the effect font.
* Improved some options, especially the color choosers.

#### Update 1.3

* Added Enhance and Dark monsters under ArcDesLHK's request.
* Fixed some bugs.
* Redone some templates to improve them.
* Added a few more templates for Anime cards.
* Tried to enhance the quality of saved image... sincerely I don't know how much I succeeded!

#### Update 1.3.1

* Fixed some bugs.
* Redone the way Rare (and above) names are rendered. Now more customizations are possible and the result should be better.

#### Update 1.3.2

* Fixed a regression I caused with update 1.3.1.

#### Update 2.0
* Redone several parts of the program (most of the edits aren't visible ones, but that will help me with future updates)
* Added the possibility to choose between Series 9 and Series 10 templates.
* Added Evolute and Evolute Conjoint monsters (as Series 9 templates) under melika567 and ArcDesLHK suggestions and with SuperBlooman permission.
* Added the possibility to choose some foils to apply to the card.
* Added the possibility to insert the "Speed Duel" overlay.
* Bug fixes.

#### Update 2.1

* Added some templates for Manga cards (Virus, Trap Spell and Wicked God) provided by Neo-RedRanger and made by MillenniumShadow.
* Added Cryplify and Prism monsters under melika567's suggestion.
* Added templates for my Update and Corrupted monsters.
* Changed the way Special characters are handled. Now there's a proper text section where to write them so I won't have to support them 1 by 1.
* Added the possibility to remove the Anubis Eye.
* Preparation for future updates!
* Minor bug fixes.

#### Update 2.1.1

* Fixed a regression I caused with update 2.1.

#### Update 2.2

* Added some custom card types (Link Traps, Link Trap Spells).
* Rewrote the ATK/DEF lines, mainly to allow the creation of cards with longer ATK/DEF lines (Numeronius Numeronia I'm looking at you!).
* Updated Upgrade/Corruption Templates.
* Added an help button for custom templates that brings you to the respective mechanic explanation.

#### Update 2.3

* Added Link Pendulums and Link Deck masters.
* Changed the way the pendulum box size is decided, now there's a slider.
* Fixed gold rarity for Deck master and link monsters.

#### Update 2.4

* Changed the way Link Pendulums and Link Deck Masters are made.
* Added King cards by DragonRikaZangetsu.
* Some bug fixes.

#### Update 2.5

* Added possibility to choose a portion of image to be used for the artwork.
* Fixed King cards (double click on [King card] a 2nd time to be able to choose the 2nd symbol).

#### Update 2.6

* Added some custom card types (Bronze, Silver, Gold, Excel, Spectrum), templates provided by Neo-RedRanger.
* Added very basic support to write YgoPro scripts. I'll add more things later!
* Minor bug fixes.

#### Update 2.6.1

* Improved the image cropper. At least now it should work!

#### Update 2.6.2

* Added Composition, Neon (only Series 10) and Hybrid (only Series 9) monsters, templates provided by Neo-RedRanger.
* Added Platinum rare support.
* Added platinum and gold rare full-art cards support.
* Some bug fixes.

#### Update 2.7

* Added Manga cards! Some templates provided by Neo-RedRanger, some others made by me.

#### Update 2.8

* Added Splice symbol to be used with Spell/Trap cards (useful for Hybrid monsters).
* Fixed Neon monsters star alignment.
* Finally managed to add a decent Ghost rarity!

#### Update 2.9

* Added Shield symbol to be used for ([Shield Spell/Trap cards](https://lionheartking.fandom.com/wiki/Shield_Spell/Trap_Cards))
* Added Series 10 Hybrid monsters, Evolution monsters, Unleash monsters (templates by Neo-RedRanger)
* Graphically improved some parts of the window.

#### Update 2.9.1

* Added series 10 Prism monsters.
* Added all the missing full-art versions.

#### Update 2.10

* Improved the Type/Ability line, now it has auto-completion.

#### Update 2.10.1

* Added some missing subtypes I forgot to add in 2.10 (Dark Tuner, Base)

#### Update 2.10.2

* Added Bandai OGC Types
* Added Plus, Minus and Armor subtypes.
* Fixed a problem that was causing the artworks to not load.

#### Update 2.11

* Added Phasm Monsters

#### Update 2.12

* Added Void attribute
* Reworked some things under the hood!

#### Update 3.0 - beta Rush Duel support

* Added Beta support for Rush Duel cards

#### Update 3.1 - Rush Duel fixes

* Code refactoring
* Bug Fixes

#### Update 3.1.1 - more Rush Duel Fixes!

* Bug Fixes

#### Update 3.2 - Under the Hood rework

* Code refactoring
* Bug Fixes

#### Update 3.2.1 - minor fixes

* Completed code refactoring
* Minor bug fixes

#### Update 3.2.2 - more Rush templates

* Added some Rush Duel templates

#### Update 3.3

* Fixed Rush duel templates (they are still temporary)
* Added Zero monsters

#### Update 3.3.1

* Translated the attribute images

#### Update 3.4

* Added the possibilty to write bold and italic text in effect boxes
* Fixed the Link number font (I'm not sure if it's the correct one even now!)
* Fixed some colors
* Added Arcade Symbol (requested by [ElitePlayer1993](https://www.deviantart.com/eliteplayer1993))

#### Update 3.4.1

* Improved a little bit font rendering

#### Update 3.5

* Updated Rush templates (some are still missing/incomplete for lack of information)
* Improved the Image cropper 

#### Update 3.5.1

* Fixed effect text position
* Fixed pendulum/deck master effect text position

#### Update 3.5.2

* Changed the way the (pendulum) effect text is shrinked

#### Update 3.5.3

* Added more freedom in the way the (pendulum) effect text is shrinked

#### Update 3.5.4 - Proper outlines!

* Changed Rush LV text outline
* Changed ATK/DEF text outline

#### Update 3.5.4.1

* Enlarged Rush ATK/DEF Text
* Fixed Rush ATK/DEF not disappearing

#### Update 3.5.5 - Better outlined texts

* Improved Rush Level/ATK/DEF Text
* Made possible to write effect/lore with bigger font sizes

#### Update 3.6 - Symbols everywhere!

* Improved the way special symbols (●, ∞, ①, ☆, @, ...) are handled

#### Update 3.6.1 - Symbols font

* Changed the font used for Symbols
* Added font for @ on TGC cards ([This one!](https://yugioh.fandom.com/wiki/Doshin_@Ignister))

#### Update 3.6.2 - Text editor

* Added a proper text editor to edit texts with more freedom
* Removed some options as they are now doable using the text editor
* Added Duel Terminal rarity

#### Update 3.6.3 - Legends!

* Added more customization options
* Added Legend cards

#### Update 3.6.4 - Gradient Names

* Added Name Gradient
* Bug fixes

#### Update 3.7 - Customizations everywhere!

* Enabled customization of more parts of the card.

#### Update 3.7.1

* Bug fixes.

#### Update 3.7.2

* Bug fixes and optimizations.

#### Update 3.8 - Rush S/T Symbols

* Updated Rush Spell/Trap cards to use symbols

#### Update 3.9 - Special Names

* Given a use the the 'Special' tab in the text editor!

#### Update 3.9.1 - Small fixes

* Various fixes
* Added some Special text choices

#### Update 3.9.2 - More fixes

* Various fixes

#### Update 3.10 - Speed Spells

* Added Speed Spell overlay
* Legend is now usable with all the Series

#### Update 3.11 - Maximum Monsters

* Added Maximum monsters
* Legend is now silver colored on non-monster cards
* Updated Rush attributes (credits to [9558able](https://www.deviantart.com/9558able/art/Rush-Duel-Template-Standard-Version-839698879))

#### Update 3.11.1 - Minor UI fixes

* Text editor is now always on top of main window
* Added some choices in special tab of Text Editor

#### Update 3.12 - Custom cards to a whole new level!

* Added generic custom cards.
* Removed custom card types now obtainable using the new custom type.

#### Update 3.12.1

* Fixed a bug with ATK and DEF not showing properly when switching from Rush to Series9 and 10

#### Update 3.13 - 4Kids

* Added 4Kids Series.

#### Update 3.13.1

* Added 4Kids template for Ra with egyptian chant.

#### Update 3.13.2

* Fixed a problem with Rush Duel ATK and DEF alignements introduced with update 3.13.

#### Update 3.13.3

* Added Pendulum and Deck Master for 4Kids Series.

#### Update 3.13.4

* Added 4kids Z-Arc templates.

#### Update π

* Added Maximum monsters to Manga, Series 9 and Series 10.
* Added possibility to create custom cards with Links.
* Fixed attribute chooser position for 4Kids.
* Added possibility to use custom attributes.
* Added possibility to use custom S/T symbols.
* Revamped the Type/Ability line, now it's possible to manually add S/T symbols by right clicking.

#### Update 3.14.1

* ATK, DEF and Link labels now can be customized.

#### Update 3.15

* Monster Type and Ability selector returned in all of its glory!

#### Update 3.15.1

* Moved S/T symbol into the new tooltip selector.

#### Update 3.15.2

* Fixed a crash that was occurring when deleting the "Made with YCM" text and checking the Pendulum box.